package main

import (
	"log"
)

func fatalError(err error, msg string) {
	if err == nil {
		return
	}
	log.Panicf("%s: %s", msg, err)
}
