package models

type ArticleStatistics struct {
	ReadTime       uint32 `json:"read_time" bson:"read_time"`
	SourceReadTime uint32 `json:"source_read_time" bson:"source_read_time"`
	TimeSave       uint32 `json:"time_save" bson:"time_save"`
}

type ArticleMetadata struct {
	SourceHost  string `json:"source_host" bson:"source_host"`
	ProcessedAt uint64 `json:"processed_at" bson:"processed_at"`
}

type Article struct {
	Anchor     string            `json:"anchor" bson:"anchor"`
	Category   string            `json:"category" bson:"category"`
	Title      string            `json:"title" bson:"title"`
	Text       string            `json:"text" bson:"text"`
	Image      string            `json:"image,omitempty" bson:"image,omitempty"`
	Source     string            `json:"source" bson:"source"`
	Statistics ArticleStatistics `json:"statistics" bson:"statistics"`
	Meta       ArticleMetadata   `json:"meta,omitempty" bson:"meta,omitempty"`
}
