package models

type NotificationRequest struct {
	Action string `json:"action" validate:"min=1"`
}

// TODO: add regexp to check email
type SendLoginSecretRequest struct {
	NotificationRequest
	Session string `json:"session" validate:"nonzero,min=1"`
	Email   string `json:"email" validate:"nonzero,min=3"`
	Secret  string `json:"secret" validate:"nonzero,min=1"`
}

// TODO: add regexp to check email
type SendAccountVerificationRequest struct {
	NotificationRequest
	Session string `json:"session" validate:"nonzero,min=1"`
	Email   string `json:"email" validate:"nonzero,min=3"`
	Secret  string `json:"secret" validate:"nonzero,min=1"`
	Expires uint64 `json:"expires" validate:"nonzero,min=1"`
}
