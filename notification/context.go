package main

import "go.mongodb.org/mongo-driver/mongo"

type Context struct {
	DatabaseUrl string
	Database    *mongo.Client
}
