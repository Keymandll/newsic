package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"golang.org/x/net/html"
)

func findArticle(node *html.Node) *html.Node {
	if node.Type == html.ElementNode && node.Data == "article" {
		return node
	}
	var article *html.Node = nil
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		article = findArticle(child)
		if article != nil {
			return article
		}
	}
	return nil
}

func findParagrahs(node *html.Node, paragraphs []*html.Node) {
	if node.Type == html.ElementNode && node.Data == "p" {
		paragraphs = append(paragraphs, node)
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		findParagrahs(child, paragraphs)
	}
}

func extractArticleText(node *html.Node) string {
	var paragraphs []*html.Node
	findParagrahs(node, paragraphs)
	log.Printf("Found %d paragraphs", len(paragraphs))
	/*
		var text = ""
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			log.Printf("XXXX: %s", child.Data) // TODO
			if node.Type != html.ElementNode || node.Data != "p" {
				continue
			}
			text += child.FirstChild.Data
		}
	*/
	var text = ""
	return text
}

func getArticle(url string) (string, error) {
	response, err := http.Get(url)
	if err != nil {
		errmsg := fmt.Sprintf("Failed to load article from '%s': %s", url, err.Error())
		return "", errors.New(errmsg)
	}
	defer response.Body.Close()
	doc, err := html.Parse(response.Body)
	if err != nil {
		errmsg := fmt.Sprintf("Failed to process HTML from '%s': %s", url, err.Error())
		return "", errors.New(errmsg)
	}
	node := findArticle(doc)
	if node.Type != html.ElementNode || node.Data != "article" {
		errmsg := fmt.Sprintf("Failed to process HTML from '%s': could not find article HTML tag", url)
		return "", errors.New(errmsg)
	}
	return extractArticleText(node), nil
}

func processArticle(url string) error {
	log.Printf("Processing article '%s'", url)
	start_time := time.Now().Unix()
	text, err := getArticle(url)
	if err != nil {
		log.Printf(err.Error())
	}
	log.Printf(text)
	// TODO
	end_time := time.Now().Unix() - start_time
	log.Printf("Processing article took %d seconds", end_time)
	return nil
}
