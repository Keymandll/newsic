package main

import (
	"testing"
)

func Test_parseNotificationMessage(t *testing.T) {
	var notificationRequest string = "{\"action\": \"unittest\"}"
	message := []byte(notificationRequest)
	result, err := parseNotificationMessage(message)
	if err != nil {
		t.Errorf("Method returned an error: %s", err.Error())
	}
	if result.Action != "unittest" {
		t.Errorf("Method returned an object with an invalid `action` property: %s.", result.Action)
	}
}

func Test_parseNotificationMessage_NoAction(t *testing.T) {
	var notificationRequest string = "{}"
	message := []byte(notificationRequest)
	_, err := parseNotificationMessage(message)
	if err == nil {
		t.Errorf("Method accepted a notification request without the `action` property set.")
	}
}

func Test_parseSendLogicSecretRequest(t *testing.T) {
	var notificationRequest string = "{\"action\": \"send_login_secret\", \"session\": \"sessionid\", \"email\":\"emailaddress\", \"secret\":\"supersecret\"}"
	message := []byte(notificationRequest)
	result, err := parseSendLogicSecretRequest(message)
	if err != nil {
		t.Errorf("Method returned an error: %s", err.Error())
	}
	if result.Action != "send_login_secret" {
		t.Errorf("Method returned an object with an invalid `action` property: %s.", result.Action)
	}
	if result.Email != "emailaddress" {
		t.Errorf("Method returned an object with an invalid `email` property: %s.", result.Action)
	}
	if result.Secret != "supersecret" {
		t.Errorf("Method returned an object with an invalid `secret` property: %s.", result.Action)
	}
}

func Test_handleIncomingRequest(t *testing.T) {
	context := Context{
		DatabaseUrl: "",
	}
	notificationRequest := "{\"action\": \"send_login_secret\", \"session\": \"sessionid\", \"email\":\"devnull@example.org\", \"secret\":\"supersecret\"}"
	message := []byte(notificationRequest)
	result := handleIncomingRequest(&context, message)
	if result != nil {
		println(result.Error())
	}
}
