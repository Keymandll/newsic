package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/smtp"
	"newsic/notification/models"
	"os"
	"strings"

	"gopkg.in/validator.v2"
)

var nameToHandlerMap = make(map[string]func(*Context, []byte) error)

func init() {
	registerHandler("send_login_secret", handleSendLoginSecretRequest)
	registerHandler("send_account_activation", handleAccountActivation)
}

func registerHandler(name string, handler func(*Context, []byte) error) {
	nameToHandlerMap[name] = handler
}

func parseMessage[T any](value []byte, result T) (T, error) {
	if err := json.Unmarshal([]byte(value), &result); err != nil {
		return result, err
	}
	if errs := validator.Validate(result); errs != nil {
		return result, errs
	}
	return result, nil
}

func parseNotificationMessage(value []byte) (models.NotificationRequest, error) {
	var response models.NotificationRequest
	return parseMessage(value, response)
}

func parseSendLogicSecretRequest(value []byte) (models.SendLoginSecretRequest, error) {
	var response models.SendLoginSecretRequest
	return parseMessage(value, response)
}

func parseAccountVerificationRequest(value []byte) (models.SendAccountVerificationRequest, error) {
	var response models.SendAccountVerificationRequest
	return parseMessage(value, response)
}

func sendEmail(recipient string, subject string, htmlBody string) error {
	address := os.Getenv("SMTP_SERVER")
	if len(address) == 0 {
		return errors.New("SMTP server address is not set using `SMTP_SERVER` environment variable.")
	}
	port := os.Getenv("SMTP_SERVER_PORT")
	if len(port) == 0 {
		return errors.New("SMTP server port is not set using `SMTP_SERVER_PORT` environment variable.")
	}
	username := os.Getenv("SMTP_USERNAME")
	if len(username) == 0 {
		return errors.New("SMTP server username is not set using `SMTP_USERNAME` environment variable.")
	}
	password := os.Getenv("SMTP_PASSWORD")
	if len(password) == 0 {
		return errors.New("SMTP server password is not set using `SMTP_PASSWORD` environment variable.")
	}
	mail_from := os.Getenv("MAIL_FROM_ADDRESS")
	if len(mail_from) == 0 {
		return errors.New("Sender email is not set using `MAIL_FROM_ADDRESS` environment variable.")
	}

	auth := smtp.PlainAuth(
		"",
		username,
		password,
		address,
	)

	from := "From: " + mail_from + "\n"
	emailTo := "To: " + recipient + "\n"
	replyTo := "Reply-To: No-Reply <no-reply@newsic.io>\n"
	emailSubject := "Subject: " + subject + "\n"
	emailMime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := []byte(from + emailTo + replyTo + emailSubject + emailMime + htmlBody)
	to := []string{recipient}

	if err := smtp.SendMail(address+":"+port, auth, mail_from, to, msg); err != nil {
		return err
	}
	return nil
}

func getTemplatesPath() (string, error) {
	templatesPath, pathError := os.Getwd()
	if pathError != nil {
		return "", pathError
	}
	location := []string{templatesPath, "templates"}
	return strings.Join(location, string(os.PathSeparator)), nil
}

func loadTemplateFile(templateName string) (string, error) {
	templatesPath, pathError := getTemplatesPath()
	if pathError != nil {
		return "", pathError
	}

	location := []string{templatesPath, templateName + ".html"}
	templateFile := strings.Join(location, string(os.PathSeparator))

	f, err := os.Open(templateFile)
	if err != nil {
		return "", err
	}
	fileStat, err := f.Stat()
	if err != nil {
		return "", err
	}
	data := make([]byte, fileStat.Size())
	_, readError := f.Read(data)
	if readError != nil {
		return "", readError
	}
	return string(data), nil
}

func handleAccountActivation(serviceContext *Context, request []byte) error {
	result, err := parseAccountVerificationRequest(request)
	if err != nil {
		return err
	}
	templateData, err := loadTemplateFile("generic_active")
	if err != nil {
		return err
	}
	hours := result.Expires / 60 / 60
	text := "Activate your account by clicking on the button below. Please note that this verification email is only valid for " + fmt.Sprintf("%d", hours) + "hours."
	actionLink := "https://newsic.io/activate?session=" + result.Session + "&secret=" + result.Secret
	templateData = strings.Replace(templateData, "%%TEXT%%", text, -1)
	templateData = strings.Replace(templateData, "%%ACTION_BUTTON_LINK%%", actionLink, -1)
	templateData = strings.Replace(templateData, "%%ACTION_BUTTON_TEXT%%", "Sign In", -1)
	templateData = strings.Replace(templateData, "%%REASON%%", "you signed up for an account.", -1)

	if err := sendEmail(result.Email, "Account Activation", templateData); err != nil {
		return err
	}
	return nil
}

func handleSendLoginSecretRequest(serviceContext *Context, request []byte) error {
	result, err := parseSendLogicSecretRequest(request)
	if err != nil {
		return err
	}
	templateData, err := loadTemplateFile("generic_active")
	if err != nil {
		return err
	}

	actionLink := "https://newsic.io/signin?session=" + result.Session + "&secret=" + result.Secret

	templateData = strings.Replace(templateData, "%%TEXT%%", "You can sign in to your account by clicking on the button below.", -1)
	templateData = strings.Replace(templateData, "%%ACTION_BUTTON_LINK%%", actionLink, -1)
	templateData = strings.Replace(templateData, "%%ACTION_BUTTON_TEXT%%", "Sign In", -1)
	templateData = strings.Replace(templateData, "%%REASON%%", "signing in requires you to click on the button above.", -1)

	if err := sendEmail(result.Email, "Login Credentials", templateData); err != nil {
		return err
	}
	return nil
}

func handleIncomingRequest(serviceContext *Context, request []byte) error {
	result, err := parseNotificationMessage(request)
	if err != nil {
		errmsg := fmt.Sprintf("Failed to process notification request: %s", err.Error())
		return errors.New(errmsg)
	}
	handler := nameToHandlerMap[result.Action]
	if handler == nil {
		errmsg := fmt.Sprintf("No action handler found for requested action: %s", result.Action)
		return errors.New(errmsg)
	}
	return handler(serviceContext, request)
}
