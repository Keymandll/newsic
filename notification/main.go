package main

import (
	"fmt"
	"log"
	"os"

	amqp "github.com/rabbitmq/amqp091-go"
)

func main() {
	queue_url := os.Getenv("QUEUE_URL")
	if len(queue_url) == 0 {
		log.Panicf("The `QUEUE_URL` environment variable must be set.")
	}
	var context = Context{}
	conn, err := amqp.Dial(queue_url)
	fatalError(err, "Failed to connect to Message Queue")
	defer conn.Close()

	ch, err := conn.Channel()
	fatalError(err, "Failed to open a channel")
	defer ch.Close()

	_, err = ch.QueueDeclare(
		"notification", // name
		false,          // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	fatalError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		"notification", // queue
		"",             // consumer
		true,           // auto-ack
		false,          // exclusive
		false,          // no-local
		false,          // no-wait
		nil,            // args
	)
	fatalError(err, "Failed to register a consumer")

	var forever chan struct{}

	go func() {
		for d := range msgs {
			if err := handleIncomingRequest(&context, d.Body); err != nil {
				fmt.Printf("%s", err.Error())
			}
		}
	}()

	log.Printf("Waiting for messages. To exit press CTRL+C")
	<-forever
}
