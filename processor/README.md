# Environment Variables

```
export SERVICE_PORT=5555
export REDIS_URL="redis://localhost:6379"
export DATABASE_URL="mongodb://127.0.0.1:27017/newsic"
export QUEUE_URL="amqp://127.0.0.1"
export OPENAI_KEY="<PASTE_HERE>"
```

## Redis Setup

```
docker run -d --name newsic-redis -p 127.0.0.1:6379:6379 redis/redis-stack-server:latest
```

If you want the Redis web UI as well for testing and development:

```
docker run -d --name newsic-redis -p 127.0.0.1:6379:6379 -p 127.0.0.1:8001:8001 redis/redis-stack:latest
```
