import Router from '@koa/router';
import parser from '../../../bodyParser';

import ServiceException from '../../../ServiceException';

const router = new Router();

import handler from '../../../article/handler';

router.post('/v1/read', parser, async (ctx) => {
    try {
        ctx.body = await handler(ctx.request.body);
    } catch (err: any) {
        if (err instanceof ServiceException) {
            console.log(err);
            ctx.throw(err.responseCode, err.errorMessage);
        }
        console.log(`Encountered unexpected error: ${err.toString()}`);
        ctx.throw(500, Error('Unexpected error'));
    }
});

export default router;
