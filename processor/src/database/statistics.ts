/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import mongoose from 'mongoose';
const { Schema } = mongoose;

const Statistics = new Schema(
    {
        _id: {
            type: String,
            required: true,
        },
        articles: {
            type: Number,
            default: 0,
        },
        total_read_time: {
            type: Number,
            default: 0,
        },
        time_saved: {
            type: Number,
            default: 0,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

Statistics.set('toJSON', {
    transform(_doc: any, ret: any) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    },
});

export default mongoose.model('Statistics', Statistics);
