/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import mongoose from 'mongoose';
const { Schema } = mongoose;

import { v4 as uuidv4 } from 'uuid';

const Article = new Schema(
    {
        _id: {
            type: String,
            default: uuidv4,
        },
        anchor: {
            type: String,
            required: true,
        },
        category: {
            type: String,
            required: true,
        },
        title: {
            type: String,
            required: true,
        },
        text: {
            type: String,
            required: true,
        },
        tags: {
            type: Schema.Types.Mixed,
        },
        image: {
            type: String,
        },
        source: {
            type: String,
            required: true,
            unique: true,
        },
        statistics: {
            type: Schema.Types.Mixed,
        },
        meta: {
            type: Schema.Types.Mixed,
        },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

Article.set('toJSON', {
    transform(_doc: any, ret: any) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    },
});

export default mongoose.model('Article', Article);
