export interface Category {
    _id: string;
    title: string;
    color: string;
}

export const categories: Category[] = [
    { _id: 'world', title: 'World', color: '#FFB300' },
    { _id: 'business', title: 'Business', color: '#6D4C41' },
    { _id: 'politics', title: 'Politics', color: '#E53935' },
    { _id: 'science', title: 'Science', color: '#7CB342' },
    { _id: 'technology', title: 'Technology', color: '#1E88E5' },
    { _id: 'uncategorised', title: 'Uncategorised', color: '#757575' },
];
