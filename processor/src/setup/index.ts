import DatabaseModelCategory from '../database/category';
import CacheModelCategory from '../cache/category';

import type { Category } from './config';
import { categories } from './config';

const ensureCategoryInCache = async (category: Category) => {
    console.log(`Ensure category exists in cache: ${category.title}`);
    let cached = null;
    const repository = await CacheModelCategory();
    try {
        cached = await repository
            .search()
            .where('_id')
            .equals(category._id)
            .return.first();
    } catch (err: any) {
        throw Error(`Failed to look up category in cache: ${err.toString()}`);
    }
    if (cached && cached._id) {
        console.log(`Category '${category.title}' already exists in cache.`);
        return;
    }
    try {
        console.log(`Saving category '${category.title}' in cache.`);

        const item = await repository.createAndSave({
            _id: category._id,
            title: category.title,
            color: category.color,
        });
        console.log(
            `Category '${category.title}' saved in cache with ID ${item._id}`
        );
    } catch (err: any) {
        throw Error(`Failed to save category in cache: ${err.toString()}`);
    }
};

const ensureCategoryInDatabase = async (category: Category) => {
    console.log(`Ensure category exists in database: ${category.title}`);
    try {
        const existing = await DatabaseModelCategory.findById(category._id);
        if (existing && existing._id === category._id) return;
    } catch (err: any) {
        throw Error(
            `Failed to check if category "${
                category._id
            }" exists: ${err.toString()}`
        );
    }
    try {
        await DatabaseModelCategory.create(category);
    } catch (err: any) {
        throw Error(
            `Failed to create category "${category._id}": ${err.toString()}`
        );
    }
};

const setupCategories = async () => {
    for (const category of categories) {
        await ensureCategoryInDatabase(category);
        await ensureCategoryInCache(category);
    }
};

const databaseSetup = async () => {
    try {
        await setupCategories();
    } catch (err: any) {
        console.log(`Failed to set up database: ${err.toString()}`);
        process.exit(1);
    }
};

export default databaseSetup;
