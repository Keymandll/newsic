import { get } from 'lodash';
import queueServer from './service/queue';
import apiServer from './service/api';

const instanceMap = {
    api: apiServer,
    queue: queueServer,
};

const getInstanceType = () => {
    const args = process.argv.slice(2);
    const instanceType = args[0];
    if (!instanceType) return null;
    return get(instanceMap, [instanceType], null);
};

(async () => {
    const handler = getInstanceType();
    if (!handler) {
        console.log(
            '[error] First argument representing the service instance type is expected to be `api` or `queue`.'
        );
        process.exit(1);
    }
    await handler();
})();
