import { get } from 'lodash';
import Koa from 'koa';

import Database from './components/database';
import routeRead from '../routes/read/v1';

const app = new Koa();
app.use(routeRead.routes()).use(routeRead.allowedMethods());

const serviceMain = async () => {
    const port = get(process, ['env', 'SERVICE_PORT'], 5555);
    const databaseUrl = get(process, ['env', 'DATABASE_URL'], '');
    if (databaseUrl.length === 0) {
        throw Error('The `DATABASE_URL` environment variable must be set.');
    }
    const db = new Database(databaseUrl);
    await db.start();
    console.log(`Service listening on port ${port}.`);
    app.listen(port);
};

export default serviceMain;
