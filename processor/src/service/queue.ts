import { get } from 'lodash';

import Database from './components/database';
import type { ProcessRequest } from '../models/processrequest';
import Queue from './components/queue';
import ServiceException from '../ServiceException';

import handler from '../article/handler';
import type { Channel, ConsumeMessage } from 'amqplib';

const databaseUrl = get(process, ['env', 'DATABASE_URL'], '');
const queueUrl = get(process, ['env', 'QUEUE_URL'], '');

if (databaseUrl.length === 0) {
    throw Error('The `DATABASE_URL` environment variable must be set.');
}

if (queueUrl.length === 0) {
    throw Error('The `QUEUE_URL` environment variable must be set.');
}
const mq = new Queue(queueUrl);
const db = new Database(databaseUrl);

let nextRun = 0;
let processedCount = 0;
const countPerInterval = 2;

mq.on(
    'process_article',
    async (
        channel: Channel,
        queueMessage: ConsumeMessage,
        message: ProcessRequest
    ) => {
        if (Math.ceil(Date.now() / 1000) < nextRun) {
            channel.nack(queueMessage, undefined, true);
            return;
        }
        try {
            await handler(message);
            processedCount += 1;
        } catch (err: any) {
            if (err instanceof ServiceException) {
                channel.ack(queueMessage);
                console.log(err);
                return;
            }
            channel.nack(queueMessage, undefined, true);
            console.log(err.toString());
            return;
        }
        channel.ack(queueMessage);
        if (processedCount > countPerInterval) {
            nextRun = Math.ceil(Date.now() / 1000) + 60;
            processedCount = 0;
        }
    }
);

const serviceMain = async () => {
    console.log('Service started');
    await db.start();
    await mq.start();
};

export default serviceMain;
