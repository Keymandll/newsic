import { Client } from 'redis-om';

console.log('Connecting to cache.');
const client = new Client().open(process.env['REDIS_URL']);

export default client;
