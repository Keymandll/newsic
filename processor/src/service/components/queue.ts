import { get } from 'lodash';
import { connect as connectMq, Channel } from 'amqplib';

import ServiceException from '../../ServiceException';

class Queue {
    url: string;
    queue: string;
    send_channel: Channel | null;
    actionToHandlerMap: Map<string, Function>;

    constructor(url: string) {
        this.url = url;
        this.queue = 'processor';
        this.send_channel = null;
        this.actionToHandlerMap = new Map<string, Function>();
    }

    on(action: string, handler: Function) {
        this.actionToHandlerMap.set(action, handler);
    }

    handleIncoming(channel: Channel, message: any) {
        if (!message) return;
        try {
            const data = JSON.parse(message.content.toString());
            const action = get(data, ['action'], null);
            const handler = this.actionToHandlerMap.get(action);
            if (!handler) {
                throw Error('Invalid action requested');
            }
            Promise.resolve(handler(channel, message, data)).then(() => {});
        } catch (err: any) {
            if (err instanceof ServiceException) {
                console.log({
                    error: `Failed (${err.responseCode}) to process article`,
                    details: err.errorMessage,
                    message: message.content.toString(),
                });
                return;
            }
            console.log({
                error: 'Failed to process message',
                details: err.toString(),
                message: message.content.toString(),
            });
        }
    }

    async start() {
        const conn = await connectMq(this.url);
        this.send_channel = await conn.createChannel();
        const listen = await conn.createChannel();
        await listen.assertQueue(this.queue, { durable: true });
        console.log('Service listening for messages');
        listen.consume(this.queue, (msg) => this.handleIncoming(listen, msg), {
            noAck: false,
        });
    }

    async send(queue: string, message: any) {
        if (!this.send_channel) {
            throw Error(
                'Failed to send message to queue: channel unavailable.'
            );
        }
        this.send_channel.sendToQueue(
            queue,
            Buffer.from(JSON.stringify(message))
        );
    }
}

export default Queue;
