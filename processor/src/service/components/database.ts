import mongoose from 'mongoose';
import setupDatabase from '../../setup';

class Database {
    url: string;
    customHandlers: any;

    constructor(url: string) {
        this.url = url;
        mongoose.set('strictQuery', false);
        mongoose.Promise = global.Promise;
        mongoose.connection.on('error', this.handleError);
        mongoose.connection.once('open', this.handleOpen);
    }

    handleError(err: any) {
        console.log('Could not connect to the database. Terminating...');
        console.log(err);
        process.exit();
    }

    async handleOpen() {
        console.log('Successfully connected to the database');
        await setupDatabase();
    }

    async start() {
        await mongoose.connect(this.url, {
            ssl: false,
            sslValidate: false,
        });
    }
}

export default Database;
