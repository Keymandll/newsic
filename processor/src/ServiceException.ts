class ServiceException {
    responseCode: number;
    errorMessage: string;
    articleUrl?: string | undefined | null;

    constructor(
        responseCode: number,
        errorMessage: string,
        artcleUrl?: string | undefined | null
    ) {
        this.responseCode = responseCode;
        this.errorMessage = errorMessage;
        this.articleUrl = artcleUrl;
    }
}

export default ServiceException;
