import { Article, ArticleStatistics } from './article';

test('Article statistics properties', () => {
    const stats: ArticleStatistics = {
        read_time: 0,
        source_read_time: 1,
        time_save: 2,
    };
    expect(stats.read_time).toBe(0);
    expect(stats.source_read_time).toBe(1);
    expect(stats.time_save).toBe(2);
});

test('Article instantiation', () => {
    const stats: ArticleStatistics = {
        read_time: 0,
        source_read_time: 1,
        time_save: 2,
    };
    const article = new Article(
        'title',
        'Multiline\ntext for testing.',
        new URL('http://localhost/'),
        'none',
        stats,
        'test',
        []
    );

    expect(article.category).toEqual('test');
    expect(article.title).toEqual('title');
    expect(article.text).toEqual('Multiline text for testing.');
    expect(article.image).toEqual('none');
    expect(article.source.toString()).toEqual('http://localhost/');

    expect(article.statistics?.read_time).toBe(0);
    expect(article.statistics?.source_read_time).toBe(1);
    expect(article.statistics?.time_save).toBe(2);

    expect(article.meta?.source_host).toEqual('localhost');
    expect(article.meta).toBeDefined();
    expect(article.meta?.processedAt).toBeGreaterThan(0);
});
