export interface ArticleStatistics {
    read_time: number;
    source_read_time: number;
    time_save: number;
}

interface ArticleMetadata {
    source_host: string;
    processedAt: number;
}

interface ArticleContent {
    category?: string;
    anchor: string;
    title: string;
    text: string;
    tags?: string[] | undefined | null;
    image: string | null;
    source: URL;
    statistics: ArticleStatistics | null;
    meta?: ArticleMetadata;
}

export class Article implements ArticleContent {
    category?: string;
    anchor: string;
    title: string;
    text: string;
    tags?: string[] | undefined | null;
    image: string | null;
    source: URL;
    statistics: ArticleStatistics | null;
    meta?: ArticleMetadata;

    constructor(
        title: string,
        text: string,
        source: URL,
        image: string | null,
        statistics: ArticleStatistics | null,
        category: string | null,
        tags: string[] | null
    ) {
        if (category) {
            this.category = category;
        }
        this.title = title.replaceAll('\n', '').trim();
        this.title = this.title.replace(/(^"|"$)/gm, '');

        const date = new Date();
        const articleDate = `${date.getFullYear()}_${
            date.getMonth() + 1
        }_${date.getDate()}`;
        this.anchor = this.title
            .split('')
            .filter((c) => c.match(/^[a-zA-Z0-9\s]$/))
            .join('');
        this.anchor = `${articleDate}-${this.anchor.replaceAll(' ', '_')}`;

        this.text = text.replaceAll('\n', ' ').trim();
        this.tags = tags;
        this.image = image;
        this.source = source;
        this.statistics = statistics;

        this.meta = {
            source_host: source.hostname,
            processedAt: Date.now(),
        };
    }
}
