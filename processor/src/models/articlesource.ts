export interface ArticleSource {
    category: string;
    url: string;
    location?: string | undefined | null;
}
