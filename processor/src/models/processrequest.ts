export interface ProcessRequest {
    action: string;
    category?: string;
    url: string;
}
