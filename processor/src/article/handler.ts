import { getHtmlElement, isValidUrl, getReadTimeSeconds } from './utils';
import {
    fetch,
    getArticleImage,
    extractTextContent,
    getArticleCategory,
} from './article';
import { generateArticle } from './openai';

import shouldIgnoreArticle from './ignore';

import { Article, ArticleStatistics } from '../models/article';

import ArticleModel from '../database/article';
import StatisticsModel from '../database/statistics';

import CacheModelCategory from '../cache/category';
import {
    articleProcessingStart,
    articleProcessingStop,
} from '../cache/articlesource';

const updateStatistics = async (
    total_read_time: number,
    time_saved: number
) => {
    const statId = '00000000-0000-0000-0000-000000000000';
    const stat = await StatisticsModel.findById(statId);
    if (!stat) {
        await StatisticsModel.create({
            _id: statId,
            articles: 1,
            total_read_time,
            time_saved,
        });
        return;
    }
    await StatisticsModel.updateOne(
        { _id: statId },
        {
            articles: stat.articles + 1,
            total_read_time: stat.total_read_time + total_read_time,
            time_saved: stat.time_saved + time_saved,
        }
    );
};

const getCategoryFromCache = async (categoryId: string) => {
    const repository = await CacheModelCategory();
    try {
        const cached = await repository
            .search()
            .where('_id')
            .is.equalTo(categoryId)
            .returnFirst();
        return cached;
    } catch (err: any) {
        return null;
    }
};

const handler = async (message: any) => {
    let { url, category } = message;
    url = isValidUrl(url);
    url.hash = '';
    const urlString = url.toString();

    const cachedSourceId = await articleProcessingStart(urlString);
    if (!cachedSourceId) {
        console.log(`Article is already being processed: ${urlString}`);
        return;
    }

    try {
        category = getArticleCategory(category);

        const cachedCategory = await getCategoryFromCache(category);

        if (!cachedCategory || !cachedCategory._id) {
            throw Error(`Unsupported category: '${category}'.`);
        }

        const startTime = Date.now() / 1000;

        const dbArticle = await ArticleModel.findOne({ source: urlString });
        if (dbArticle) {
            return dbArticle;
        }
        const dom = await fetch(urlString);
        const html = getHtmlElement(dom);
        const articleImage = getArticleImage(html);
        const articleContent = extractTextContent(url, html);

        if (shouldIgnoreArticle(url, articleContent)) {
            return;
        }
        console.log(`Processing article: ${urlString}`);

        const article = await generateArticle(url, articleContent);
        let parsedArticleData = null;
        try {
            parsedArticleData = JSON.parse(article);
        } catch(err: any) {
            console.log(`Failed to process article data returned by OpenAI: ${err.toString()}`);
            console.log(`Article data: ${article}`);
            throw err;
        }

        const processingTimeSec = Date.now() / 1000 - startTime;
        const read_time = getReadTimeSeconds(article);
        const source_read_time = getReadTimeSeconds(articleContent);
        const time_save = Math.ceil(
            source_read_time - (read_time + processingTimeSec)
        );

        const stats: ArticleStatistics = {
            read_time,
            source_read_time,
            time_save,
        };

        let articleData = null;
        try {
            articleData = new Article(
                parsedArticleData.title,
                parsedArticleData.text,
                url,
                articleImage,
                stats,
                parsedArticleData.category.toLowerCase(),
                parsedArticleData.tags
            );
        } catch (err: any) {
            if (err.code === 11000) {
                return;
            }
            throw err;
        }
        await updateStatistics(stats.read_time, stats.time_save);
        return ArticleModel.create(articleData);
    } finally {
        await articleProcessingStop(cachedSourceId);
    }
};

export default handler;
