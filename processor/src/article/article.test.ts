import { JSDOM } from 'jsdom';
import { getArticleImage, extractTextContent } from './article';
import { getHtmlElement } from './utils';

const domDataNoImage =
    '<html><body><article><p>text1</p><p>text2</p></article></body></html>';
const domDataWithImage =
    '<html><head><meta property="og:image" content="https://example.org/test.png" /></head><body><article><p>text1</p><p>text2</p></article></body></html>';

test('get article image from DOM', () => {
    const dom = new JSDOM(domDataWithImage);
    const html = getHtmlElement(dom);
    const result = getArticleImage(html);
    expect(result).toEqual('https://example.org/test.png');
});

test('get article text from DOM', () => {
    const url = new URL('http://example.org/test');
    const dom = new JSDOM(domDataNoImage);
    const html = getHtmlElement(dom);
    const result = extractTextContent(url, html);
    expect(result).toEqual('text1 text2');
});
