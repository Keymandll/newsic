import { JSDOM, ResourceLoader, FetchOptions, AbortablePromise } from 'jsdom';
import ServiceException from '../ServiceException';

export const getArticleImage = (htmlElement: HTMLElement | null) => {
    if (!htmlElement) return null;
    const head = htmlElement.querySelector('head');
    if (!head) {
        return null;
    }
    let image = null;
    head.querySelectorAll('meta').forEach((item) => {
        if (item.getAttribute('property') === 'og:image') {
            image = item.getAttribute('content');
        }
    });
    return image;
};

export const getArticleCategory = (category: string): string => {
    if (typeof category != 'string' || category.length === 0) {
        return 'uncategorised';
    }
    return category;
};

export const extractTextContent = (
    url: URL,
    htmlElement: HTMLHtmlElement
): string => {
    let articleText = '';

    const article = htmlElement.querySelector('article');
    if (!article) {
        throw new ServiceException(
            400,
            'Document has no article tag',
            url.href
        );
    }
    const paragraphs = article.querySelectorAll('p');
    if (paragraphs.length === 0) {
        throw new ServiceException(400, 'Article has no paragraphs', url.href);
    }
    paragraphs.forEach((item) => {
        articleText += `${item.textContent} `;
    });
    return articleText.trim();
};

class CustomResourceLoader extends ResourceLoader {
    // @ts-ignore
    override fetch(
        url: string,
        options: FetchOptions
    ): AbortablePromise<Buffer> | Promise<Buffer> | null {
        if (url.endsWith('.js') || url.endsWith('.css')) {
            return Promise.resolve(Buffer.from(''));
        }
        return super.fetch(url, options);
    }
}

export const fetch = async (url: string) => {
    const resourceLoader = new CustomResourceLoader();
    try {
        const dom = await JSDOM.fromURL(url, {
            // @ts-ignore
            resources: resourceLoader,
            pretendToBeVisual: true,
            includeNodeLocations: true,
        });
        return dom;
    } catch (err: any) {
        throw new ServiceException(
            400,
            `Failed to load article from URL: ${err.toString()}`,
            url
        );
    }
};
