import type { JSDOM } from 'jsdom';
import { encode } from 'gpt-3-encoder';

import ServiceException from '../ServiceException';

/**
 * Return the number of GPT-3 tokens a piece of text costs.
 *
 * `4097` is the maximum supported by the model, however, the `gpt-3-encoder`
 * library tends to be off by about `300` so we add `300` to the result to
 * be safe.
 *
 * @param value The text to calculate the token count of
 * @returns The token count
 */
export const getTokenCount = (value: string) => {
    return encode(value).length + 300;
};

export const isValidUrl = (text: string): URL => {
    let url = null;
    try {
        url = new URL(text);
    } catch (err) {
        throw new ServiceException(400, 'Invalid URL');
    }
    if (!text.startsWith('https://')) {
        throw new ServiceException(400, 'Unsupported URL protocol scheme');
    }
    return url;
};

export const getHtmlElement = (dom: JSDOM): HTMLHtmlElement => {
    const result = dom.window.document.querySelector('html');
    if (!result) {
        throw new ServiceException(400, 'Document missing html tag');
    }
    return result;
};

export const getOpenAIOptions = (prompt: string, max_tokens: number) => ({
    model: 'text-davinci-003',
    prompt,
    temperature: 0.9,
    max_tokens,
    top_p: 1,
    frequency_penalty: 0,
    presence_penalty: 0,
});

export const getReadTimeSeconds = (text: string | null) => {
    if (!text || text.length === 0) return 0;
    const avgWordsPerMinute = 200 / 60;
    const wordsCount = text.split(' ').length;
    return Math.ceil(wordsCount / avgWordsPerMinute);
};
