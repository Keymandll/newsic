import { Configuration, OpenAIApi } from 'openai';
import { get } from 'lodash';

import { getOpenAIOptions, getTokenCount } from './utils';
import ServiceException from '../ServiceException';



const instructions = "Remove opinions, questions, explanations, hypothesis, speculation, suggestions, critiques, quotes from the article below and Summarize the result in a maximum one minute read focusing only on current and past events.";

const contentFilter = () => {
    return `${instructions} Return the result as the "text" property of a valid JSON object prepended with "//==>" that has the following additional properties:
"title": A short, maximum 8 words long title for the below article.
"category": The best fitting category from: world, business, technology, science, politics.
Avoid using double quotes in the property values.\n\n`;
};

const max_tokens = 4096;
const PROCESS_ARTICLE = contentFilter();

const API_KEY = get(process, ['env', 'OPENAI_KEY'], '');

const configuration = new Configuration({
    apiKey: API_KEY,
});
const openai = new OpenAIApi(configuration);

const runCompletion = async (
    url: URL,
    request: string,
    text: string
): Promise<string> => {
    const articleText = text.replace('\n', ' ').trim();
    const prompt = `${request}\n\n${articleText}\n\n`;
    const token_count = getTokenCount(prompt);

    if (token_count >= max_tokens) {
        throw new ServiceException(
            400,
            'Maximum article size exceeded',
            url.href
        );
    }

    const completion_tokens = max_tokens - token_count;
    // TODO: should we check if the remaining tokens are too low to worth
    // a try?
    const options = getOpenAIOptions(prompt, completion_tokens);
    let completion = null;
    try {
        completion = await openai.createCompletion(options);
    } catch (err: any) {
        const status = get(err, ['response', 'status'], null);
        const error = get(err, ['response', 'data', 'error'], null);
        throw new ServiceException(status, error.message, url.href);
    }
    if (!completion) {
        throw new ServiceException(
            400,
            'AI refused to serve request',
            url.href
        );
    }
    const choices = get(completion, ['data', 'choices'], null);
    if (!Array.isArray(choices) || choices.length === 0) {
        throw new ServiceException(
            400,
            'AI returned with unexpected data',
            url.href
        );
    }

    let articleData: string | undefined = get(choices[0], ['text'], '');

    articleData = articleData.split('//==>')[1];
    if (!articleData) {
        throw new ServiceException(
            400,
            'AI did not return the requested JSON object',
            url.href
        );
    }
    if (articleData.includes(instructions) || articleData.includes(instructions.substring(0, 40))) {
        throw new ServiceException(
            400,
            'The idiot AI leaked the instructions in the response again',
            url.href
        );
    }

    return articleData.trim();
};

export const generateArticle = async (
    url: URL,
    originalText: string
): Promise<string> => {
    return runCompletion(url, PROCESS_ARTICLE, originalText);
};
