const ignoreList = [
    { urlIncludes: 'independent.co.uk', contentIncludes: 'digest' },
    { urlIncludes: 'nytimes.com', contentIncludes: 'briefing' },
];

const shouldIgnoreArticle = (url: URL, rawArticleContent: string) => {
    for (const toIgnore of ignoreList) {
        if (
            url.toString().includes(toIgnore.urlIncludes) &&
            rawArticleContent
                .toLowerCase()
                .includes(toIgnore.contentIncludes.toLowerCase())
        ) {
            console.log(
                `Ignoring article based on ignore rule: ${url.toString()}`
            );
            return true;
        }
    }
    return false;
};

export default shouldIgnoreArticle;
