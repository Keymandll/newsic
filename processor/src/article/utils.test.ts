import { JSDOM } from 'jsdom';
import { isValidUrl, getHtmlElement, getOpenAIOptions } from './utils';

test('check if valid URL', () => {
    let url = 'https://example.org/index?param=value#test';
    expect(isValidUrl(url)).toBe(true);

    url = 'https://example.org/index?param=value';
    expect(isValidUrl(url)).toBe(true);

    url = 'http://example.org/index?param=value';
    expect(isValidUrl(url)).toBe(false); // We only support HTTPS

    url = 'https:/example.org';
    expect(isValidUrl(url)).toBe(false);
});

test('get HTML element from DOM', () => {
    const dom = new JSDOM('<html><body></body></html>');
    const result = getHtmlElement(dom);
    expect(result?.tagName.toLowerCase()).toEqual('html');
});

test('get OpenAI options', () => {
    const options = getOpenAIOptions('test-prompt', 1234);
    expect(options.model).toEqual('text-davinci-003');
    expect(options.prompt).toEqual('test-prompt');
    expect(options.temperature).toEqual(0.9);
    expect(options.max_tokens).toEqual(1234);
    expect(options.top_p).toEqual(1);
    expect(options.frequency_penalty).toEqual(0);
    expect(options.presence_penalty).toEqual(0);
});
