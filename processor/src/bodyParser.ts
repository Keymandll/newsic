import { koaBody } from 'koa-body';

// eslint-disable-next-line import/prefer-default-export
const parser = koaBody({
    jsonLimit: '5mb',
    formLimit: '5mb',
    textLimit: '5mb',
});

export default parser;
