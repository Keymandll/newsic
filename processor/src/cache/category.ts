import { Entity, Schema } from 'redis-om';
import client from '../service/components/cache';

interface Category {
    _id: any;
    title: string;
    color: string;
}

class Category extends Entity {}

const categorySchema = new Schema(Category, {
    _id: { type: 'string' },
    title: { type: 'string' },
    color: { type: 'string' },
});

const getCategoryRepository = async () => {
    const c = await client;
    const categoryRepository = c.fetchRepository(categorySchema);
    await categoryRepository.createIndex();
    return categoryRepository;
};

export default getCategoryRepository;
