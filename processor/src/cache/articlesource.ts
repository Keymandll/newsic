import { isString } from 'lodash';
import { Entity, Repository, Schema } from 'redis-om';
import client from '../service/components/cache';

interface ArticleSource {
    url: string;
    category: string;
    location?: string | undefined | null;
}

class ArticleSource extends Entity {}

const articleSourceSchema = new Schema(ArticleSource, {
    url: { type: 'string' },
    category: { type: 'string' },
});

const getArticleSourceRepository = async () => {
    const c = await client;
    const repository = c.fetchRepository(articleSourceSchema);
    try {
        await repository.createIndex();
    } catch (err) {} // Ignore
    return repository;
};

const getArticleSource = async (
    repository: Repository<ArticleSource>,
    url: string
) => {
    try {
        return repository.search().where('url').is.equalTo(url).returnFirst();
    } catch (err: any) {
        return null;
    }
};

/**
 * Check if the article is already queued for processing (or being processed).
 * If it's processing, we should ignore it. If it's not, we should push the
 * article source details into the cache to signal that it's been already
 * picked up by the processor.
 *
 * @param articleSource ArticleSource object
 * @returns
 */
export const articleProcessingStart = async (url: string) => {
    const repository = await getArticleSourceRepository();
    try {
        const articleFromCache = await getArticleSource(repository, url);
        if (articleFromCache && isString(articleFromCache.entityId))
            return null;
    } catch (err: any) {
        console.log(
            `Failed to check article source in cache: ${err.toString()}`
        );
        return null;
    }

    try {
        const cached = await repository.createAndSave({ url });
        if (!cached || !isString(cached.entityId)) return null;
        const ttlInSeconds = 0.5 * 60 * 60; // 30 minutes
        await repository.expire(cached.entityId, ttlInSeconds);
        return cached.entityId;
    } catch (err: any) {
        console.log(`Failed to cache article source: ${err.toString()}`);
        return null;
    }
};

/**
 * Remove article source from cache.
 *
 * @param articleSource ArticleSource object
 * @returns
 */
export const articleProcessingStop = async (entityId: string | null) => {
    if (!entityId) return;
    const repository = await getArticleSourceRepository();
    await repository.remove(entityId);
};
