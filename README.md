# Newsic

## About

[ PoC | MVP | Social Experiment ]

Today's news are 90% opinions, suggestions, speculations, questions, fables, gossip, fearmongering, political propaganda, brainwashing, hidden or direct advertisements and whatnot. Newsic uses machine learning (GTP-3 from OpenAI) to filter out the noise and present you with short, unopinionated, factual news.

This project was a social experiment. Please note that things were put together quickly just to get something out there. I did not want to invest too much into something people may not care about at all. Long story short: literally no one cared.

This project is very dangerous and can negatively impact your mental health if you dig deep enough to learn what and why I implemented. You will see a very dystopian world opening up in front of your eyes.

## Services

| Service                        | Description                       |
| ------------------------------ | --------------------------------- |
| [notification](./notification) | Simple notification service       |
| [crawler](./crawler)           | RSS feed crawler                  |
| [processor](./processor)       | Service to process news articles  |
| [frontend](./frontend)         | The frontend (web user interface) |

## Setup

### Crawler

Update [crawler/src/sources.ts](./crawler/src/sources.ts) with the news sources (RSS feeds) you would like to be processed periodically.

### Frontend

Update the following environment variables in [frontend/Dockerfile](./frontend/Dockerfile) to match your environment.

 * NEXT_SECRET_RECAPTCHA_SITE_KEY
 * JWT_CRYPTO_SECRET
 * MANAGER_EMAIL_ADDRESS

### Notification

Update the following environment variables in [notification/Dockerfile](./notification/Dockerfile) to match your environment.

 * SMTP_SERVER
 * SMTP_SERVER_PORT
 * SMTP_USERNAME
 * SMTP_PASSWORD
 * MAIL_FROM_ADDRESS

### Processor

Update the following environment variables in [processor/Dockerfile](./processor/Dockerfile) to match your environment.

 * OPENAI_KEY

## Build

Visit the `.gitlab-ci.yml` file of each service and uncomment the commented lines. This enables building the services using GitLab runner.

The build container images are stored in the (GitLab) Container Registry.

## Deployment

### Docker Compose Configuration

You must update the image source locations in the [docker-compose.yml](./deploy/docker-compose.yml) file to match your environment.

As you can see, when the project was alive the service images were fetched from `registry.gitlab.com/newsic-news/${service_name}:master`, where `${service_name}` is a placelholder for the name of the service (e.g., "crawler").

### Deploy Nginx Proxy

Deploy Nginx on the host the Newsic services will be running on. Nginx will serve as a reverse proxy. 

You can use the [included configuration file](./deploy/nginx.conf) as a base to set up the proxy. Make sure to update everything in there to match your environment.

### Start Up Services

Execute `docker compose up` from the [deploy](./deploy) directory.

## Security

Do not worry about any credentials you may find in this repository. They won't work.
