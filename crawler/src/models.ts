export interface ArticleSource {
    category: string;
    url: string;
}
