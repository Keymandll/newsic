import { connect as connectMq, Connection, Channel } from 'amqplib';

class Queue {
    url: string;
    queue: string;
    connection: Connection | null;
    send_channel: Channel | null;

    constructor(url: string) {
        this.url = url;
        this.queue = 'dataservice';
        this.connection = null;
        this.send_channel = null;
    }

    async start() {
        this.connection = await connectMq(this.url);
        this.send_channel = await this.connection.createChannel();
    }

    async stop() {
        this.send_channel?.close();
    }

    async send(queue: string, message: any) {
        if (!this.send_channel) {
            throw Error("Failed to send message to queue: channel unavailable.");
        }
        this.send_channel.sendToQueue(
            queue,
            Buffer.from(JSON.stringify(message))
        );
    }
}

export default Queue;
