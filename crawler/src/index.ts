import { get } from 'lodash';
import type { ArticleSource } from './models';
import Parser from 'rss-parser';
const parser = new Parser();

import Queue from './queue';
import sources from './sources';

const queueUrl = get(process, ['env', 'QUEUE_URL'], '');

let shouldStop = false;

if (queueUrl.length === 0) {
    throw Error("The `QUEUE_URL` environment variable must be set.");
}

console.log(`Service started.`);

const mq = new Queue(queueUrl);

interface ProcessArticleRequest {
    action: string,
    category: string,
    url: string,
}

const processFeedItem = async (category: string, url: string | undefined) => {
    if (!url) return;
    console.log(`[${category}] url: "${url}".`);
    let data: ProcessArticleRequest = {
        action: 'process_article',
        category: category,
        url: url,
    };
    mq.send('processor', data);
}

const isToday = (postDate: string) => new Date().setHours(0,0,0,0) == new Date(postDate).setHours(0,0,0,0);

const sleep = (seconds: number) => {
    return new Promise(resolve => setTimeout(resolve, seconds * 1000));
}

const processSource = async (source: ArticleSource) => {
    try {
        const feed = await parser.parseURL(source.url);
        for (const item of feed.items) {
            const itemDate = item.pubDate;
            if (!itemDate) continue;
            if (!isToday(itemDate)) continue;
            await processFeedItem(source.category, item.link);
        };
    } catch (err: any) {
        console.log(`Failed to process source '${source.url}': ${err.toString()}`)
    }
}

process.on('SIGINT', function () {
    console.log("Caught interrupt signal");
    shouldStop = true;
});

(async () => {
    let nextCheck = 0;
    await mq.start();
    while (!shouldStop) {
        const currentDate = Date.now();
        if (Date.now() <= nextCheck) {
            await sleep(1);
            continue;
        }
        for (const source of sources) {
            await processSource(source);
        }
        // Grab URLs every 1 hour
        nextCheck = currentDate + (60 * 60 * 1000);
    }
    await mq.stop();
    process.exit(0);
})();
