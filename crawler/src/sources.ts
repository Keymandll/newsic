import type { ArticleSource } from "./models";

// Categories are set up by the `processor` service and defined
// in `src/setup/config.ts` of the processor codebase.
const sources: Array<ArticleSource> = [
    { category: 'world', url: 'http://feeds.bbci.co.uk/news/world/rss.xml' },
    { category: 'business', url: 'http://feeds.bbci.co.uk/news/business/rss.xml' },
    { category: 'technology', url: 'http://feeds.bbci.co.uk/news/technology/rss.xml' },
    { category: 'science', url: 'http://feeds.bbci.co.uk/news/science_and_environment/rss.xml' },
    { category: 'politics', url: 'http://feeds.bbci.co.uk/news/politics/rss.xml' },
];

/*
    { category: 'business', url: 'https://rss.nytimes.com/services/xml/rss/nyt/Business.xml' },
    { category: 'technology', url: 'https://rss.nytimes.com/services/xml/rss/nyt/Technology.xml' },
    { category: 'science', url: 'https://rss.nytimes.com/services/xml/rss/nyt/Science.xml' },
*/

export default sources;
