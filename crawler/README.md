# Environment Variables

```bash
export QUEUE_URL="amqp://127.0.0.1"
```

# RabbitMQ

```bash
docker run -p 127.0.0.1:5672:5672 --name newsic-mq -d rabbitmq:3
```
