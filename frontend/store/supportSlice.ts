import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from 'next-redux-wrapper';

export interface SupportState {
  supportVisible: boolean;
  emailAddress: string;
}

const initialState: SupportState = {
  supportVisible: true,
  emailAddress: '',
};

export const supportSlice = createSlice({
  name: "support",
  initialState,
  reducers: {
    setVisible(state, action) {
      state.supportVisible = action.payload;
    },
    setEmailAddress(state, action) {
      state.emailAddress = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state: any, action: any) => {
      return {
        ...state,
        ...action.payload
      };
    })
  },
});

export const { setVisible, setEmailAddress } = supportSlice.actions;

export default supportSlice.reducer;
