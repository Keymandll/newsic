import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from 'next-redux-wrapper';

export interface SearchState {
  searchEnabled: boolean;
  searchText: string;
}

const initialState: SearchState = {
  searchEnabled: false,
  searchText: '',
};

export const searchSlice = createSlice({
  name: "search",
  initialState,
  reducers: {
    setSearchEnabled(state, action) {
      state.searchEnabled = action.payload;
    },
    setSearchText(state, action) {
      state.searchText = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state: any, action: any) => {
      return {
        ...state,
        ...action.payload
      };
    })
  },
});

export const { setSearchEnabled, setSearchText } = searchSlice.actions;

export default searchSlice.reducer;
