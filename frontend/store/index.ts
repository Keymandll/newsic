import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import { searchSlice } from "./searchSlice";
import { filterSlice } from "./filterSlice";
import { categorySlice } from "./categorySlice";
import { supportSlice } from "./supportSlice";
import { createWrapper } from "next-redux-wrapper";

const makeStore = () =>
  configureStore({
    reducer: {
      [searchSlice.name]: searchSlice.reducer,
      [filterSlice.name]: filterSlice.reducer,
      [categorySlice.name]: categorySlice.reducer,
      [supportSlice.name]: supportSlice.reducer,
    },
    devTools: true,
  });

export type AppStore = ReturnType<typeof makeStore>;
export type AppState = ReturnType<AppStore["getState"]>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action
>;

export const wrapper = createWrapper<AppStore>(makeStore);
