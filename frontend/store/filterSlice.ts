import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from 'next-redux-wrapper';

export interface FilterState {
  filterEnabled: boolean;
}

const initialState: FilterState = {
    filterEnabled: false,
};

export const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    setFilterEnabled(state, action) {
      state.filterEnabled = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state: any, action: any) => {
      return {
        ...state,
        ...action.payload
      };
    })
  },
});

export const { setFilterEnabled } = filterSlice.actions;

export default filterSlice.reducer;
