import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from 'next-redux-wrapper';

export interface CategoryState {
  categories: string[];
}

const initialState: CategoryState = {
  categories: ['all'],
};

export const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    setCategories(state, action) {
      state.categories = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state: any, action: any) => {
      return {
        ...state,
        ...action.payload
      };
    })
  },
});

export const { setCategories } = categorySlice.actions;

export default categorySlice.reducer;
