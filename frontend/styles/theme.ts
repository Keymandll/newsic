import { ThemeOptions } from "@mui/material";
import { orange } from '@mui/material/colors';

const themeOptions: ThemeOptions = {
    palette: {
        mode: 'dark',
        primary: {
            main: orange[700],
            dark: 'rgb(237, 112, 45)',
        },
        secondary: {
            main: '#F5F5F5',
            light: '#FAFAFA',
            dark: '#BDBDBD',
        },
        background: {
            default: '#1a1a1a',
            paper: '#171717',
        },
    },
    typography: {
        fontSize: 12,
    },
    shape: {
        borderRadius: 2,
    },
    components: {
        MuiAppBar: {
            styleOverrides: {
                root: {
                    backgroundColor: '#000000',
                },
            },
        },
        MuiPaper: {
            styleOverrides: {
                root: {
                    backgroundColor: '#090909',
                },
            },
        },
        MuiTabs: {
            styleOverrides: {
                indicator: {
                    backgroundColor: '#ffffff',
                },
            },
        },
    },
};

export default themeOptions;
