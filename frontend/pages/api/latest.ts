// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

import clientPromise from '../../core/lib/mongodb';
import type Article from '../../models/Article';

type Error = {
    message: string,
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Article[]|Error>
) {
    const client = await clientPromise;
    const db = client.db("newsic");

    const latest = await db
        .collection("articles")
        .find({})
        .sort({ createdAt: -1 })
        .project({
            _id: 1,
            anchor: 1,
            title: 1,
            category: 1,
            statistics: 1,
            createdAt: 1
        })
        .limit(3)
        .toArray();
    let result: Article[] = [];
    if (latest && latest.length > 0) {
        result = JSON.parse(JSON.stringify(latest)); 
    }
    res.status(200).json(result);
}
