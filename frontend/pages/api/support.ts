// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { get, isString } from 'lodash';
import type { NextApiRequest, NextApiResponse } from 'next';

import clientPromise from '../../core/lib/mongodb';
import { verifyCaptchaToken } from '../../utils/captcha';

type Support = {
    result: string,
}

type Error = {
    message: string,
}

const validation = {
    email: {
        regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        errorMessage: 'Invalid email address.',
    },
};

const isInvalid = (type: string, value: string) => {
    if (value.length === 0) return false;
    const regexp = get(validation, [type, 'regexp'], null);
    if (!regexp) return false;
    return !regexp.test(value);
};

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Support|Error>
) {
    if (req.method !== 'POST') {
        const response: Error = { message: 'Invalid request' };
        res.status(400).json(response);
        return;
    }
    if (
        !isString(req.body.email) ||
        req.body.email.length === 0 ||
        isInvalid('email', req.body.email)
    ) {
        const response: Error = { message: 'Invalid email address' };
        res.status(400).json(response);
        return;
    }
    if (!isString(req.body.token) || req.body.token.length === 0) {
        const response: Error = { message: 'Invalid captcha token' };
        res.status(400).json(response);
        return;
    }

    const emailAddress = req.body.email.trim().toLowerCase();

    const result = await verifyCaptchaToken(req.body.token);
    if (!result) {
        const response: Error = { message: 'Invalid captcha token' };
        res.status(400).json(response);
        return;
    }

    const client = await clientPromise;
    const db = client.db("newsic");

    const existing = await db.collection("support").findOne({ email: emailAddress });
    if (existing) {
        res.status(200).json({ result: 'success' });
        return;
    }

    const doc = await db.collection("support").insertOne({ email: emailAddress });

    if (!doc) {
        const response: Error = { message: 'Unexpected error' };
        res.status(500).json(response);
        return;
    }

    res.status(200).json({ 'result': 'success' });
}
