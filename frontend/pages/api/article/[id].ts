// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { get, isString } from 'lodash';
import type { NextApiRequest, NextApiResponse } from 'next';
import jwt from 'jsonwebtoken';

import clientPromise from '../../../core/lib/mongodb';

type Error = {
    message: string,
}

type Result = {
    status: boolean,
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Result|Error>
) {
    if (req.method !== 'DELETE') {
        const response: Error = { message: 'Invalid request' };
        res.status(400).json(response);
        return;
    }

    const apiKey = get(req, ['cookies', 'access-token'], null);

    if (!isString(apiKey) || apiKey.length === 0) {
        console.log('Authorization check failure: API key missing from request.');
        const response: Error = { message: 'Unauthorized' };
        res.status(401).json(response);
        return;
    }
    if (!process.env.JWT_CRYPTO_SECRET) {
        console.log('Authorization check failure: JWT secret is not set in ENV.');
        const response: Error = { message: 'Unauthorized' };
        res.status(401).json(response);
        return;
    }

    let authResult = null;
    try {
        authResult = jwt.verify(apiKey, process.env.JWT_CRYPTO_SECRET, {
            algorithms: ['HS512']
        });
    } catch (err: any) {
        res.setHeader('Set-Cookie', 'access-token=; Secure; HttpOnly; SameSite=Strict; max-age=0');
        console.log(`API key verification failure during authorization: ${err.toString()}`);
        const response: Error = { message: 'Unauthorized' };
        res.status(401).json(response);
        return;
    }

    if (get(authResult, ['role'], null) !== 'manager') {
        console.log('Authorization check failure: `manager` role required.');
        const response: Error = { message: 'Unauthorized' };
        res.status(401).json(response);
        return;
    }

    const articleId = get(req, ['query', 'id'], null);
    if (!articleId) {
        const response: Error = { message: 'Invalid request' };
        res.status(400).json(response);
        return;
    }

    const client = await clientPromise;
    const db = client.db("newsic");

    const article = await db
        .collection("articles")
        .findOne({ _id: articleId });

    if (!article) {
        const response: Error = { message: 'Article not found' };
        res.status(404).json(response);
        return;
    }

    await db.collection("articles").deleteOne({ _id: articleId });

    const statId = '00000000-0000-0000-0000-000000000000';
    const stats = await db
        .collection("statistics")
        .findOne({ _id: statId });
    if (!stats) {
        const response: Error = { message: 'Failed to update statistics' };
        res.status(500).json(response);
        return;
    }
    await db.collection("statistics").updateOne({ _id: statId }, { $set: {
        articles: stats.articles - 1,
        total_read_time: stats.total_read_time - article.statistics.read_time,
        time_saved: stats.time_saved - article.statistics.time_save,
    }});

    res.status(200).json({ status: true });
}
