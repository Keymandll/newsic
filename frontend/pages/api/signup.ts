// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

import User from '../../core/modules/user';
import ServiceError from '../../core/modules/error';

type Result = {
    result: boolean,
}

type Error = {
    message: string,
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Result|Error>
) {
    const { email, token } = req.body;
    try {
        const user = new User(email);
        await user.signup(token);
    } catch(err: any) {
        if (err instanceof ServiceError) {
            console.log(err.internalMessage);
            res.status(err.statusCode).json({
                message: err.externalMessage
            });
            return;
        }
        console.log(`Unexpected error during sign up request: ${err.toString()}`);
        res.status(500).json({
            message: 'An unexpected error occurred. Please try again later.'
        });
        return;
    }

    res.status(200).json({ result: true });
}
