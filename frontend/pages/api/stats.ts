// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

import clientPromise from '../../core/lib/mongodb';

type Statistics = {
    articles: number,
    total_read_time: number,
    time_saved: number,
}

type Error = {
    message: string,
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Statistics|Error>
) {
    const statId = '00000000-0000-0000-0000-000000000000';
    const client = await clientPromise;
    const db = client.db("newsic");

    const stat = await db
        .collection("statistics")
        .findOne({ _id: statId });

    if (!stat) {
        const response: Error = { message: 'Failed to fetch statistics' };
        res.status(500).json(response);
        return;
    }

    res.status(200).json({
        articles: stat.articles,
        total_read_time: stat.total_read_time,
        time_saved: stat.time_saved,
    });
}
