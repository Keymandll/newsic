// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

import User from '../../core/modules/user';
import ServiceError from '../../core/modules/error';

type Result = {
    result: boolean,
}

type Error = {
    message: string,
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Result|Error>
) {
    const { email, token } = req.body;
    try {
        const user = new User(email);
        await user.signin(token);
    } catch(err: any) {
        if (err instanceof ServiceError) {
            res.status(err.statusCode).json({ message: 'Failed to sign in.' });
            return;
        }
        res.status(500).json({ message: 'An unexpected error happened. Please try again later.' });
        return;
    }
    res.status(200).json({ result: true });
}
