import { get, isNumber, isString } from 'lodash';
import clientPromise from '../core/lib/mongodb';
import { InferGetServerSidePropsType } from 'next';
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from 'next/router';
import { useCallback } from 'react';

import jwt from 'jsonwebtoken';

import { Box } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2

import PageLayout from '../core/components/PageLayout';
import MainMenu from '../core/components/MainMenu';
import NewsicFooter from '../components/NewsicFooter';
import ArticleList from '../components/ArticleList';
import NewsPagination from '../components/NewsPagination';

import { getCategories } from '../cache/category';
import { isAuthenticated } from '../core/modules/session';

import config from '../config';
import pages from '../config/pages';
import FilterButton from '../components/FilterButton';
import CategorySelector from '../components/CategorySelector';
import MenuSearch from '../core/components/MenuSearch';

import { setCategories } from '../store/categorySlice';
import { ParsedUrlQueryInput } from 'querystring';

const getPageFromContext = (context: any, pagesTotal: number) => {
    if (!context.query) return 0;
    let { page } = context.query;
    if (page > 0) {
        page -= 1;
    }
    return !isNumber(page) || page < 0 || page > pagesTotal ? 0 : page;
}

const getQueryParamFromContext = (context: any, param: string) => {
    if (!context.query) return null;
    const parameter = get(context, ['query', param], null);
    if (!isString(parameter) || parameter.length === 0) return null;
    return parameter;
}

const getSearchQueryFromContext = (context: any) => {
    return getQueryParamFromContext(context, 'search');
}

const getFilterCategoryFromContext = (context: any) => {
    const categories = getQueryParamFromContext(context, 'categories');
    if (!categories) return ['all'];
    return categories.split(",").map((c) => c.trim());
}

export async function getServerSideProps(context: any) {
    if (!isAuthenticated(context)) {
        return {
            redirect: {
                permanent: false,
                destination: '/',
            }
        };
    }
    const articlesPerPage = 15;
    try {
        let authenticated = false;
        let role = null;
        const authCookie = context.req.cookies['access-token'];
        if (isString(authCookie) && authCookie.length > 0 && process.env.JWT_CRYPTO_SECRET) {
            try {
                const authResult = jwt.verify(authCookie, process.env.JWT_CRYPTO_SECRET, {
                    algorithms: ['HS512']
                });
                role = get(authResult, ['role'], null);
                if (role) {
                    authenticated = true;
                }
            } catch (err) {
                authenticated = false;
                role = null;
            }
        }

        const client = await clientPromise;
        const db = client.db("newsic");

        // TODO: Is this the right place to do this?
        await db.collection("articles").createIndex({
            text: "text",
            title: "text"
        });

        const search = getSearchQueryFromContext(context);
        let filter = {};
        if (search) {
            filter = {
                ...filter,
                $text:{ $search: search }
            }
        }

        const categoriesSelected = getFilterCategoryFromContext(context);

        if (categoriesSelected && !categoriesSelected.includes('all')) {
            filter = {
                ...filter,
                category: {
                    $in: categoriesSelected
                },
            }
        }

        const documentCount = await db
            .collection("articles")
            .countDocuments(filter);

        const pagesTotal = Math.ceil(documentCount / articlesPerPage);
        const page = getPageFromContext(context, pagesTotal);

        const news = await db
            .collection("articles")
            .find(filter)
            .sort({ createdAt: -1 })
            .skip(page * articlesPerPage)
            .limit(articlesPerPage)
            .toArray();

        const categories = await getCategories();

        return {
            props: {
                isConnected: true,
                authenticated,
                role,
                pagesTotal,
                currentPage: page + 1,
                news: JSON.parse(JSON.stringify(news)),
                search,
                categories: JSON.parse(JSON.stringify(categories)),
            },
        }
    } catch (e) {
        console.error(e)
        return {
            props: { isConnected: false, news: [] },
        }
    }
}

export default function News({
    isConnected,
    authenticated,
    role,
    pagesTotal,
    currentPage,
    news,
    search,
    categories,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {

    const filterEnabled: boolean = useSelector((state: any) => state.filter.filterEnabled);
    const filterCategory = useSelector((state: any) => state.category.categories);
    const router = useRouter();
    const dispatch = useDispatch();

    const getCategoryValue = () => {
        const v = get(router, ['query', 'categories'], filterCategory);
        if (Array.isArray(v)) return v;
        if (v.length === 0) return ['all'];
        return v.split(',').map((c: string) => c.trim());
    }

    const isManager = () => {
        return authenticated === true && role === 'manager';
    }

    const handleFilter = useCallback(
        (selection: string[]) => {
            dispatch(setCategories(selection));

            const query: ParsedUrlQueryInput = { 
                ...router.query,
                categories: selection.join(',')
            };
            
            if (!selection || selection.includes('all')) {
                delete query.categories;
            }
            router.push({
                pathname: '/news',
                query,
            });
        }, []
    );

    const getUtilities = () => {
        const utilities: any = [];
        if (authenticated && filterEnabled) {
            return (
                <CategorySelector
                    value={getCategoryValue()}
                    options={categories}
                    onChange={handleFilter}
                />
            );
        }
        return utilities;
    }

    const renderHead = () => {
        return (
            <MainMenu
                pages={pages}
                controls={[
                    <Box style={{ display: 'flex' }} key="controls">
                        <MenuSearch endpoint="/news" />
                        <FilterButton key="filter" />
                    </Box>
                ]}
                utilities={getUtilities()}
            />
        );
    }

    const renderBody = () => {
        return (
            <Box>
                <Grid container spacing={2}>
                    <ArticleList
                        isManager={isManager()}
                        news={news}
                        search={search}
                    />
                </Grid>
                <NewsPagination
                    newsCount={news.length}
                    pagesTotal={pagesTotal}
                    currentPage={currentPage}
                />
            </Box>
        );
    }

    return (
        <PageLayout
            pageTitle="Newsic (Beta)"
            pageDescription="Newsic: How News Meant to be Played"
            metaOgDescription="Newsic: Short, unopinionated and factual news powered by artificial intelligence."
            metaOgUrl={`https://${config.getDomain()}/`}
            metaOgImage={config.getOgImageUrl()}
            analyticsId="G-ES1KG53J54"
            head={renderHead()}
            body={renderBody()}
            footer={<NewsicFooter />}
        />
    );
}
