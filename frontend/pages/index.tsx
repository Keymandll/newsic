import { useState, useCallback } from 'react';

import {
    Box,
    Tab,
    Tabs,
    Typography,
} from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2

import PageLayout from '../core/components/PageLayout';
import MainMenu from '../core/components/MainMenu';
import NewsicFooter from '../components/NewsicFooter';
import Statistics from '../components/Statistics';
import BenefitSection from '../components/BenefitSection';

import AccessTimeIcon from '@mui/icons-material/AccessTime';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import AddReactionIcon from '@mui/icons-material/AddReaction';
import SignUpForm from '../core/components/SignUpForm';
import SignInForm from '../core/components/SignInForm';

import { isAuthenticated } from '../core/modules/session';

import config from '../config';
import pages from '../config/pages';
import LatestArticles from '../components/LatestArticles';

export async function getServerSideProps(context: any) {
    if (isAuthenticated(context)) {
        return {
            redirect: {
                permanent: false,
                destination: '/news',
            }
        };
    }
    return {
        props: { isConnected: true },
    }
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <>
                    {children}
                </>
            )}
        </div>
    );
}

export default function Home() {
    const [tabValue, setTabValue] = useState(0);

    const handleTabChange = useCallback(
        (_: React.SyntheticEvent, newValue: number) => {
            setTabValue(newValue);
        },
        []
    )

    const renderHead = () => {
        return <MainMenu pages={pages} />;
    }

    const renderBody = () => {
        return (
            <>
                <Box mt={8} mb={1} className="landing">
                    <Grid pt={2} container spacing={2}>
                        <Grid xs={12} md={6} lg={6} xl={6} p={15} className="landing-left">
                            <Box>
                                <img src={config.getLogoUrl()} width="100px" />
                                <Typography variant="h4" style={{ marginTop: 25 }}>NEWSIC: How News Meant to be Played</Typography>
                                <Typography style={{ marginBottom: 10 }}>Short, Factual, Unopinionated</Typography>
                            </Box>
                        </Grid>
                        <Grid xs={12} md={6} lg={6} xl={6} p={2}>
                            <Box m={4} p={2} style={{ backgroundColor: 'rgba(0, 0, 0, 0.25)' }}>
                                <Statistics />
                            </Box>
                            <Box m={4} p={2} style={{ backgroundColor: 'rgba(0, 0, 0, 0.25)' }}>
                                <Tabs value={tabValue} onChange={handleTabChange} aria-label="basic tabs example">
                                    <Tab label="Sign Up" />
                                    <Tab label="Sign In" />
                                </Tabs>
                                <TabPanel value={tabValue} index={0}>
                                    <SignUpForm apiEndpoint='/api/signup' />
                                </TabPanel>
                                <TabPanel value={tabValue} index={1}>
                                    <SignInForm apiEndpoint='/api/signin' />
                                </TabPanel>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
                <Box mt={9} p={5}>
                    <Grid pt={4} container spacing={10}>
                        <Grid xs={12} md={6} lg={6} xl={6}>
                            <Typography variant="h4" style={{ marginBottom: 20 }}>The News that Matters</Typography>
                            <Typography style={{ fontSize: '1.2em' }}>
                                Today's news are 90% opinions, suggestions, speculations,
                                questions, fables, gossip, fearmongering, political propaganda, brainwashing,
                                hidden or direct advertisements and whatnot. Newsic uses machine learning and
                                artificial intelligence to filter out the noise and present you with short,
                                unopinionated, factual news.
                            </Typography>
                        </Grid>
                        <Grid xs={12} md={6} lg={6} xl={6}>
                            <Typography variant="h4" style={{ marginBottom: 20 }}>Latest 3 News Articles</Typography>
                            <Typography style={{ marginBottom: 20 }}>Click on the article titles to see a preview.</Typography>
                            <LatestArticles />
                        </Grid>
                    </Grid>
                </Box>
                <Box mt={9} p={5} pb={8} style={{ textAlign: 'center', backgroundColor: '#111111' }}>
                    <Typography variant="h4" style={{ padding: 20, marginBottom: 10 }}>Simple, Comfortable, Powerful</Typography>
                    <Typography variant="h5" style={{ padding: 20, marginBottom: 30 }}>Filter your news by category and search our extensive news database using a simple and polished interface.</Typography>
                    <img src="/preview.png" className="preview-shadow" style={{ width: '80%' }} />
                </Box>
                <Box mb={5} p={5}>
                    <Grid pt={4} container spacing={6}>
                        <Grid xs={12} md={4} lg={4} xl={4}>
                            <BenefitSection
                                icon={<AccessTimeIcon style={{ color: '#f57c00', marginBottom: 20, width: 70, height: 70 }} />}
                                title="Quick"
                            >
                                On average, a news article takes 3-6 minutes to read. Thanks to
                                Newsic, you can be done in a minute without missing a thing.
                                Newsic filters out the noise and presents you with short, unopinionated,
                                factual news.
                            </BenefitSection>
                        </Grid>
                        <Grid xs={12} md={4} lg={4} xl={4}>
                            <BenefitSection
                                icon={<MenuBookIcon style={{ color: '#f57c00', marginBottom: 20, width: 70, height: 70 }} />}
                                title="Informed"
                            >
                                Spend your newly acquired extra time to learn more about what is
                                going on in the world. Be one of the best informed.
                            </BenefitSection>
                        </Grid>
                        <Grid xs={12} md={4} lg={4} xl={4}>
                            <BenefitSection
                                icon={<AddReactionIcon style={{ color: '#f57c00', marginBottom: 20, width: 70, height: 70 }} />}
                                title="Smart"
                            >
                                Consuming disarmed news allows you to form your own opinions
                                and have pleasant and meaningful discussions with others.
                            </BenefitSection>
                        </Grid>
                    </Grid>
                </Box>
            </>
        );
    }

    return (
        <PageLayout
            pageTitle="Newsic (Beta)"
            pageDescription="Newsic: How News Meant to be Played"
            metaOgDescription="Newsic: Short, unopinionated and factual news powered by artificial intelligence."
            metaOgUrl={`https://${config.getDomain()}/`}
            metaOgImage={config.getOgImageUrl()}
            analyticsId="G-ES1KG53J54"
            head={renderHead()}
            body={renderBody()}
            footer={<NewsicFooter />}
            disablePadding={true}
        />
    );
}
