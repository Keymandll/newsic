import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';

import '../styles/globals.css'

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import React, { FC } from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider, CssBaseline } from '@mui/material';

import { createTheme } from '@mui/material/styles';

import type { AppProps } from 'next/app'
import { SnackbarProvider } from 'notistack';

import { wrapper } from "../store";

import theme from '../styles/theme';

const appTheme = createTheme(theme);

const App: FC<AppProps> = ({ Component, ...rest }) => {
  const { store, props } = wrapper.useWrappedStore(rest);
  return (
    <GoogleReCaptchaProvider
      reCaptchaKey="6LeUxJIjAAAAAI8ZBylM7xV2cgnrXegjL2O2uzU3"
      scriptProps={{
        async: false, // optional, default to false,
        defer: false, // optional, default to false
        appendTo: 'head', // optional, default to "head", can be "head" or "body",
        nonce: undefined // optional, default undefined
      }}
    >
      <Provider store={store}>
        <ThemeProvider theme={appTheme}>
          <SnackbarProvider maxSnack={3}>
            <CssBaseline />
            <Component {...props.pageProps} />
          </SnackbarProvider>
        </ThemeProvider>
      </Provider>
    </GoogleReCaptchaProvider>
  );
};

export default App;
