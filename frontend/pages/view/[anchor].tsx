import clientPromise from '../../core/lib/mongodb';
import { InferGetServerSidePropsType } from 'next';

import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2

import PageLayout from '../../core/components/PageLayout';
import MainMenu from '../../core/components/MainMenu';
import FullArticle from '../../components/FullArticle';
import NewsicFooter from '../../components/NewsicFooter';

import { getPreviewText } from '../../utils/article';

import config from '../../config';
import pages from '../../config/pages';

export async function getServerSideProps(context: any) {
    try {
        const client = await clientPromise;
        const db = client.db("newsic");

        const article = await db
            .collection("articles")
            .findOne({ anchor: context.params.anchor });

        if (!article) {
            return {
                redirect: {
                    permanent: false,
                    destination: '/',
                }
            }
        }

        return {
            props: { isConnected: true, article: JSON.parse(JSON.stringify(article)) },
        }
    } catch (e) {
        console.error(e)
        return {
            props: { isConnected: false, article: null },
        }
    }
}

export default function View({
    isConnected,
    article,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {

    const renderBody = () => {
        return (
            <Grid
                container
                spacing={2}
                justifyContent="center"
                alignItems="center"
                sx={{ marginBottom: 5 }}
            >
                <Grid xs={12} md={6} lg={6} xl={4}>
                    <FullArticle article={article} />
                </Grid>
            </Grid>
        );
    }

    return (
        <PageLayout
            pageTitle={`Newsic - ${article.title}`}
            pageDescription={article.title}
            metaOgDescription={getPreviewText(article)}
            metaOgUrl={`https://${config.getDomain()}/view/${article.anchor}`}
            metaOgImage={article.image}
            analyticsId="G-ES1KG53J54"
            head={<MainMenu pages={pages} />}
            body={renderBody()}
            footer={<NewsicFooter />}
        />
    );
}
