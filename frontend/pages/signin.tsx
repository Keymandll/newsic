import { get, isString } from 'lodash';
import jwt from 'jsonwebtoken';
import { InferGetServerSidePropsType } from 'next';

import { Box, Typography } from '@mui/material';

import PageLayout from '../core/components/PageLayout';
import MainMenu from '../core/components/MainMenu';

import NewsicFooter from '../components/NewsicFooter';

import Token from '../core/modules/security/token';

import config from '../config';
import pages from '../config/pages';
import User from '../core/modules/user';

const getQueryParamFromContext = (context: any, param: string) => {
    if (!context.query) return null;
    const parameter = get(context, ['query', param], null);
    if (!isString(parameter) || parameter.length === 0) return null;
    return parameter;
}

const getSessionFromContext = (context: any) => {
    return getQueryParamFromContext(context, 'session');
}

const getSecretFromContext = (context: any) => {
    return getQueryParamFromContext(context, 'secret');
}

const logInUser = async (context: any) => {
    const props = { props: { error: '' } };
    const session = getSessionFromContext(context);
    if (!session || !isString(session)) {
        console.log('The authentication session identifier was missing from the sign-in request.');
        props.props.error = 'Invalid request.';
        return props;
    }

    if (!process.env.JWT_CRYPTO_SECRET) {
        console.log('Failed to authenticate user because JWT signing key was not found in ENV.');
        props.props.error = 'An unexpected error occurred. We are working on it. Please try later.';
        context.res.setHeader('Set-Cookie', [`access-token=; Secure; HttpOnly; SameSite=Strict; max-age=0`]);
        return props;
    }

    const secret = getSecretFromContext(context);
    if (!secret || !isString(secret)) {
        console.log('The authentication secret identifier was missing from the sign-in request.');
        props.props.error = 'Invalid request.';
        return props;
    }

    const tokenService = new Token();
    await tokenService.init();

    const valid = await tokenService.isValidSecret('signin', session, secret);
    if (!valid) {
        props.props.error = 'Invalid credentials.';
        context.res.setHeader('Set-Cookie', [`access-token=; Secure; HttpOnly; SameSite=Strict; max-age=0`]);
        return props;
    }

    const email = await tokenService.getEmail('signin', session);
    if (!email) {
        console.log('Could not find email address of user in the cache during sign in.');
        props.props.error = 'An unexpected error occurred. We are working on it. Please try later.';
        context.res.setHeader('Set-Cookie', [`access-token=; Secure; HttpOnly; SameSite=Strict; max-age=0`]);
        return props;
    }
    const user = new User(email);
    const role = await user.getRole();
    if (!role) {
        console.log('Could not fetch user role from database during sign in.');
        props.props.error = 'An unexpected error occurred. We are working on it. Please try later.';
        context.res.setHeader('Set-Cookie', [`access-token=; Secure; HttpOnly; SameSite=Strict; max-age=0`]);
        return props;
    }

    const token = jwt.sign(
        {
            role: role,
            exp: (Date.now() / 1000) + 43150,
        }, 
        process.env.JWT_CRYPTO_SECRET,
        {
            algorithm: 'HS512',
        }
    );
    context.res.setHeader('Set-Cookie', [`access-token=${token}; Secure; HttpOnly; SameSite=Strict; max-age=43150`]);
    await user.recordLoginTime();

    await tokenService.clear(session);
    return {
        redirect: {
            permanent: false,
            destination: '/news',
        }
    };
}

export async function getServerSideProps(context: any) {
    const props = { props: { error: '' } };
    if (context.req.method === 'GET') {
        return logInUser(context);
    }
    // TODO: post: handle sign-in request
    return props;

}

export default function Signin({ error }: InferGetServerSidePropsType<typeof getServerSideProps>) {

    const renderContent = () => {
        const style = {
            display: 'grid',
            minHeight: '70vh',
            alignItems: 'center',
            alignContent: 'center',
            justifyItems: 'center',
            textAlign: 'center',
        }
        let message = error.length > 0 ? error : 'You have successfully signed in. Your browser will be redirected soon.';
        return (
            <Box sx={style}>
                <Typography>
                    {message}
                </Typography>
            </Box>
        );
    }

    return (
        <PageLayout
            pageTitle="Newsic (Beta) - Sign In"
            pageDescription="Newsic: How News Meant to be Played"
            metaOgDescription="Newsic: Short, unopinionated and factual news powered by artificial intelligence."
            metaOgUrl={`https://${config.getDomain()}/`}
            metaOgImage={config.getOgImageUrl()}
            analyticsId="G-ES1KG53J54"
            head={<MainMenu pages={pages} />}
            body={renderContent()}
            footer={<NewsicFooter />}
        />
    );
}
