import { get, isString } from 'lodash';
import jwt from 'jsonwebtoken';
import { InferGetServerSidePropsType } from 'next';

import { Box, Typography } from '@mui/material';

import PageLayout from '../core/components/PageLayout';
import MainMenu from '../core/components/MainMenu';
import NewsicFooter from '../components/NewsicFooter';

import Token from '../core/modules/security/token';

import User from '../core/modules/user';

import config from '../config';
import pages from '../config/pages';

const getQueryParamFromContext = (context: any, param: string) => {
    if (!context.query) return null;
    const parameter = get(context, ['query', param], null);
    if (!isString(parameter) || parameter.length === 0) return null;
    return parameter;
}

const getSessionFromContext = (context: any) => {
    return getQueryParamFromContext(context, 'session');
}

const getSecretFromContext = (context: any) => {
    return getQueryParamFromContext(context, 'secret');
}

const clearAccessToken = (context: any) => {
    if (!context) return;
    context.res.setHeader('Set-Cookie', [`access-token=; Secure; HttpOnly; SameSite=Strict; max-age=0`]);
}

export async function getServerSideProps(context: any) {
    const props = { props: { error: '' } };
    if (context.req.method !== 'GET') {
        return props;
    }
    const session = getSessionFromContext(context);
    if (!session || !isString(session)) {
        console.log('The session identifier was missing from the account activation request.');
        props.props.error = 'Invalid request.';
        return props;
    }
    const secret = getSecretFromContext(context);
    if (!secret || !isString(secret)) {
        console.log('The secret was missing from the account activation request.');
        props.props.error = 'Invalid request.';
        return props;
    }

    const tokenService = new Token();
    await tokenService.init();

    const valid = await tokenService.isValidSecret('activate', session, secret);
    if (!valid) {
        props.props.error = 'Invalid credentials.';
        clearAccessToken(context);
        return props;
    }

    if (!process.env.JWT_CRYPTO_SECRET) {
        console.log('Failed to authenticate user because JWT signing key was not found in ENV.');
        props.props.error = 'An unexpected error occurred. We are working on it. Please try later.';
        clearAccessToken(context);
        return props;
    }

    const emailAddress = await tokenService.getEmail('activate', session);
    if (!emailAddress || !isString(emailAddress) || emailAddress.length === 0) {
        console.log('Failed to activate user account because the email address was not found in the cache.');
        props.props.error = 'An unexpected error occurred. We are working on it. Please try later.';
        clearAccessToken(context);
        return props;
    }
    let userId = null;
    try {
        const user = new User(emailAddress);
        const userId = await user.create();
        if (!userId) {
            throw Error('the user object returned was null.');
        }
    } catch (err: any) {
        console.log(`Failed to create user account: ${err.toString()}.`);
        props.props.error = 'An unexpected error occurred. We are working on it. Please try again later.';
        clearAccessToken(context);
        return props;
    }

    const token = jwt.sign(
        {
            id: userId,
            role: 'user',
            exp: (Date.now() / 1000) + 43150,
        }, 
        process.env.JWT_CRYPTO_SECRET,
        {
            algorithm: 'HS512',
        }
    );
    context.res.setHeader('Set-Cookie', [`access-token=${token}; Secure; HttpOnly; SameSite=Strict; max-age=43150`]);
    
    await tokenService.clear(session);
    return {
        redirect: {
            permanent: false,
            destination: '/',
        }
    };
}

export default function Activate({ error }: InferGetServerSidePropsType<typeof getServerSideProps>) {

    const renderContent = () => {
        const style = {
            display: 'grid',
            minHeight: '70vh',
            alignItems: 'center',
            alignContent: 'center',
            justifyItems: 'center',
            textAlign: 'center',
        }
        let message = error.length > 0 ? error : 'Account activated. Your browser will be redirected soon.';
        return (
            <Box sx={style}>
                <Typography style={{ color: '#ff0000' }}>
                    {message}
                </Typography>
            </Box>
        );
    }

    return (
        <PageLayout
            pageTitle="Newsic (Beta) - Account Activation"
            pageDescription="Newsic: How News Meant to be Played"
            metaOgDescription="Newsic: Short, unopinionated and factual news powered by artificial intelligence."
            metaOgUrl={`https://${config.getDomain()}/`}
            metaOgImage={config.getOgImageUrl()}
            analyticsId="G-ES1KG53J54"
            head={<MainMenu pages={pages} />}
            body={renderContent()}
            footer={<NewsicFooter />}
        />
    );
}
