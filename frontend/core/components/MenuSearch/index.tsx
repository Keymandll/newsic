import { get } from 'lodash';
import { FC, useState } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from "react-redux";

import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';

import { setSearchText } from '../../../store/searchSlice';
import { ParsedUrlQueryInput } from 'querystring';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
}));

interface Props {
    endpoint: string,
}

const MenuSearch: FC<Props> = ({ endpoint }) => {
    const searchText: string = useSelector((state: any) => state.search.searchText);
    const router = useRouter();
    const [text, setText] = useState(get(router, ['query', 'search'], searchText));
    const dispatch = useDispatch();

    return (
        <Search>
            <SearchIconWrapper>
                <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
                placeholder="Search…"
                inputProps={{ 'aria-label': 'search' }}
                value={text}
                onChange={(e) => setText(e.target.value)}
                onKeyUp={(e) => {
                    if (e.key !== 'Enter') return;

                    const query: ParsedUrlQueryInput = { 
                        ...router.query,
                        search: text
                    };
                    if (text.length === 0) {
                        delete query.search;
                    }
                    dispatch(setSearchText(text));
                    router.push({
                        pathname: endpoint,
                        query,
                    }); 
                }}
            />
        </Search>
    );
}

export default MenuSearch;
