import Head from 'next/head';
import { FC } from 'react';

interface Props {
    pageTitle: string,
    pageDescription: string,
    metaOgUrl: string,
    metaOgDescription: string,
    metaOgImage: string,
}

const PageHead: FC<Props> = ({
    pageTitle,
    pageDescription,
    metaOgUrl,
    metaOgDescription,
    metaOgImage,
}) => {
    return (
        <Head>
            <title>{pageTitle}</title>
            <meta name="description" content={pageDescription} />
            <link rel="icon" href="/favicon.ico" />
            <meta property="og:title" content={pageDescription} />
            <meta property="og:url" content={metaOgUrl} />
            <meta property="og:type" content="website" />
            <meta property="og:description" content={metaOgDescription} />
            <meta property="og:image" content={metaOgImage} />
        </Head>
    );
}

export default PageHead;
