import axios from 'axios';
import { FC, useState, useCallback } from 'react';

import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

import { Box, Button, TextField, Typography } from '@mui/material';

interface Props {
    apiEndpoint: string;
}

const SignUpForm: FC<Props> = ({ apiEndpoint }) => {
    const { executeRecaptcha } = useGoogleReCaptcha();
    const [email, setEmail] = useState('');
    const [token, setToken] = useState('');
    const [signedUp, setSignedUp] = useState(false);
    const [message, setMessage] = useState('');

    const displayMessage = () => {
        if (!signedUp && message.length === 0) return null;
        const props = {
            marginTop: 20,
            color: 'inherit',
        }
        if (!signedUp) {
            props.color = '#ff0000';
        }
        return <Typography style={props}>{message}</Typography>
    }

    const updateEmailAddress = useCallback(
        (event: any) => {
            setEmail(event.target.value)
        },
        []
    );

    const submitSignupRequest = useCallback(
        async () => {
            if (!executeRecaptcha) {
                console.log('Execute recaptcha not yet available');
                return;
            }
            const token = await executeRecaptcha('submitManagerAccessRequest');
            setToken(token);

            axios.post(apiEndpoint, { email, token }).then((res) => {
                setEmail('');
                setToken('');
                setSignedUp(true);
                setMessage('Please follow the instructions received in the email to activate your account.');
            }).catch((err) => {
                setSignedUp(false);
                setMessage('Failed to sign up. Please try again later.');
            });
        },
        [email, token, executeRecaptcha]
    );

    return (
        <Box mt={3}>
            {!signedUp && <><Typography style={{ marginBottom: 15 }}>
                Sign up for a free account to access the news and enjoy various features to 
                customize how you consume them.
            </Typography>
            <form id="manager-access-form" style={{ display: 'grid' }} action="/api/signup" method="POST">
                <TextField
                    fullWidth={true}
                    style={{ marginBottom: 10 }}
                    type="email"
                    variant="outlined"
                    margin="none"
                    size="small"
                    value={email}
                    onChange={updateEmailAddress}
                    placeholder="Email address"
                />
                <TextField
                    style={{ visibility: 'hidden' }}
                    type="hidden"
                    id="token"
                    name="token"
                    value={token}
                />
                <Box>
                    <Button
                        onClick={submitSignupRequest}
                        variant="contained"
                        size="medium"
                    >
                        Sign Up
                    </Button>
                </Box>
            </form></>}
            {displayMessage()}
        </Box>
    );
}

export default SignUpForm;
