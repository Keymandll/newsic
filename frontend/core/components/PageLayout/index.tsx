import { FC, ReactElement } from 'react';
import { Box } from '@mui/material';

import PageHead from '../PageHead';
import GoogleAnalytics from '../GoogleAnalytics';

interface Props {
    pageTitle: string,
    pageDescription: string,
    metaOgUrl: string,
    metaOgDescription: string,
    metaOgImage: string,
    analyticsId: string,
    disablePadding?: boolean,
    head?: ReactElement,
    body?: ReactElement,
    footer?: ReactElement,
}

const PageLayout: FC<Props> = ({
    pageTitle,
    pageDescription,
    metaOgUrl,
    metaOgDescription,
    metaOgImage,
    analyticsId,
    head,
    body,
    footer,
    disablePadding,
}) => {
    const padding = disablePadding ? 0 : 2;
    return (
        <>
            <PageHead
                pageTitle={pageTitle}
                pageDescription={pageDescription}
                metaOgDescription={metaOgDescription}
                metaOgUrl={metaOgUrl}
                metaOgImage={metaOgImage}
            />
            <>
                <GoogleAnalytics analyticsId={analyticsId} />
                {head}
                <Box mt={8} style={{ minHeight: '60vh' }}>
                    <Box p={padding} style={{ width: '100%', height: '100%' }}>
                        {body}
                    </Box>
                </Box>
                {footer}
            </>
        </>
    );
}

export default PageLayout;
