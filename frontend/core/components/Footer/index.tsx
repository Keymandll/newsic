
import { Box, Typography } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2
import { FC, ReactNode } from 'react';

interface Props {
    organizationName: string,
    contentLeft?: ReactNode | null,
    contentRight?: ReactNode | null,
}

const Footer: FC<Props> = ({ organizationName, contentLeft, contentRight }) => {
    return (
        <>
            <Box
                style={{ backgroundColor: '#202020', padding: 60 }}
            >
                <Grid container spacing={2}>
                    <Grid xs={12} md={8} lg={8} xl={8}>
                        {contentLeft}
                    </Grid>
                    <Grid xs={12} md={4} lg={4} xl={4}>
                        {contentRight}
                    </Grid>
                </Grid>
            </Box>
            <Box
                style={{ backgroundColor: '#121212', color: '#888888', fontSize: '1.0em', padding: 20 }}
                alignContent="center"
                alignItems="center"
                textAlign="center"
            >
                © {new Date().getFullYear()} {organizationName}. All rights reserved
                <Typography style={{ fontSize: '0.7em', marginTop: 8 }}>
                    This site is protected by reCAPTCHA and the Google <a className="external-link" href="https://policies.google.com/privacy">Privacy Policy</a> and <a className="external-link" href="https://policies.google.com/terms">Terms of Service</a> apply.
                </Typography>
            </Box>
        </>
    );
}

export default Footer;
