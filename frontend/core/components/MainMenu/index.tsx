import Link from 'next/link';

import * as React from 'react';
import { FC, ReactNode, useCallback } from "react";
import {
    AppBar, Box, Menu, MenuItem, Typography, IconButton, Divider
} from '@mui/material';
import Toolbar from '@mui/material/Toolbar';

import MenuIcon from '@mui/icons-material/Menu';

interface Page {
    name: string,
    link: string,
}

interface Props {
    pages?: Page[] | null,
    utilities?: ReactNode[] | ReactNode | null,
    controls?: ReactNode[] | ReactNode | null,
}

const MainMenu: FC<Props> = ({ controls, pages, utilities }) => {

    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

    const handleCloseNavMenu = useCallback(
        () => {
            setAnchorElNav(null);
        }, []
    );

    const handleOpenNavMenu = useCallback(
        (event: React.MouseEvent<HTMLElement>) => {
            setAnchorElNav(event.currentTarget);
        }, []
    );

    const showUtilityPanel = () => {
        if (Array.isArray(utilities) && utilities.length === 0) return null;
        if (!utilities) return null;
        return (
            <Box style={{ backgroundColor: '#191919' }}>
                <Divider style={{ backgroundColor: '#111111' }} />
                <Box p={3}>
                    {utilities}
                </Box>
            </Box>
        );
    }

    const renderPages = () => {
        if (!Array.isArray(pages) || pages.length === 0) return null;
        return pages.map((page) => (
            <Link href={page.link} key={page.link}>
                <MenuItem onClick={handleCloseNavMenu}>
                    <Typography textAlign="center">{page.name}</Typography>
                </MenuItem>
            </Link>
        ));
    }

    const renderMenuControls = () => {
        if (!controls) return null;
        return controls;
    }

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="fixed">
                <Toolbar>
                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            {renderPages()}
                        </Menu>
                    </Box>
                    <img src="/logo.svg" width="18" style={{ marginRight: 15 }} />
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {renderPages()}
                    </Box>
                    {renderMenuControls()}
                </Toolbar>
                {showUtilityPanel()}
            </AppBar>
        </Box>
    );
}

export default MainMenu;
