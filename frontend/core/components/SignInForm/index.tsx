import axios from 'axios';
import { FC, useState, useCallback } from 'react';

import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

import { Box, Button, TextField, Typography } from '@mui/material';

interface Props {
    apiEndpoint: string;
}

const SignInForm: FC<Props> = ({ apiEndpoint }) => {
    const { executeRecaptcha } = useGoogleReCaptcha();
    const [email, setEmail] = useState('');
    const [token, setToken] = useState('');
    const [signedIn, setSignedIn] = useState(false);
    const [message, setMessage] = useState('');

    const submitSigninRequest = useCallback(
        async () => {
            if (!executeRecaptcha) {
                console.log('Execute recaptcha not yet available');
                return;
            }
            const token = await executeRecaptcha('submitManagerAccessRequest');
            setToken(token);

            axios.post(apiEndpoint, { email, token }).then((res) => {
                setEmail('');
                setToken('');
                setSignedIn(true);
                setMessage('Please follow the instructions received in the email to sign in to your account.');
            }).catch(() => {
                setSignedIn(false);
                setMessage('Failed to sign in. Please try again later.');
            });
        },
        [executeRecaptcha, email, token]
    );

    const updateEmailAddress = useCallback(
        (event: any) => {
            setEmail(event.target.value);
        },
        []
    );

    const displayMessage = () => {
        if (!signedIn && message.length === 0) return null;
        const props = {
            marginTop: 20,
            color: 'inherit',
        }
        if (!signedIn) {
            props.color = '#ff0000';
        }
        return <Typography style={props}>{message}</Typography>
    }

    return (
        <Box mt={3}>
            {!signedIn && <>
                <form id="manager-access-form" style={{ display: 'grid' }}>
                    <TextField
                        fullWidth={true}
                        style={{ marginBottom: 10 }}
                        type="email"
                        variant="outlined"
                        margin="none"
                        size="small"
                        value={email}
                        onChange={updateEmailAddress}
                        placeholder="Email address"
                    />
                    <TextField
                        style={{ visibility: 'hidden' }}
                        type="hidden"
                        id="token"
                        name="token"
                        value={token}
                    />
                    <Box>
                        <Button
                            onClick={submitSigninRequest}
                            variant="contained"
                            size="medium"
                        >
                            Sign In
                        </Button>
                    </Box>
                </form>
            </>}
            {displayMessage()}
        </Box>
    );
}

export default SignInForm;
