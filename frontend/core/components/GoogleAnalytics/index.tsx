import Script from 'next/script';
import { FC } from 'react';

interface Props {
    analyticsId: string,
}

const GoogleAnalytics: FC<Props> = ({ analyticsId }) => {
    return (
        <>
            <Script
                src={`https://www.googletagmanager.com/gtag/js?id=${analyticsId}`}
                strategy="afterInteractive"
            />
            <Script id="google-analytics" strategy="afterInteractive">
                {`
                window.dataLayer = window.dataLayer || [];
                function gtag(){window.dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', '${analyticsId}');
                `}
            </Script>
        </>
    );
}

export default GoogleAnalytics;
