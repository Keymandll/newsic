import { get, isString } from 'lodash';
import jwt from 'jsonwebtoken';

export const isAuthenticated = (context: any) => {
    const apiKey = get(context.req, ['cookies', 'access-token'], null);

    if (!isString(apiKey) || apiKey.length === 0) {
        return null;
    }
    if (!process.env.JWT_CRYPTO_SECRET) {
        console.log('Authorization check failure: JWT secret is not set in ENV.');
        return null;
    }

    let authResult = null;
    try {
        authResult = jwt.verify(apiKey, process.env.JWT_CRYPTO_SECRET, {
            algorithms: ['HS512']
        });
    } catch (err: any) {
        return null;
    }
    return authResult;
}
