import clientPromise from '../../lib/mongodb';

import Token from '../security/token';
import { validateCaptcha, validateEmail } from '../security/validation';
import { sendNotification } from '../notification';

import ServiceError from '../error';

interface User {
    email: string;
    token: Token;
}

class UserInstance implements User {
    email: string;
    token: Token;

    constructor(email: string) {
        if (!validateEmail(email)) {
            throw new ServiceError(
                400,
                'user_instance',
                `Invalid email address: ${email}.`,
                'Invalid email address.',
            );
        }
        this.email = email;
        this.token = new Token();
    }

    /**
     * Get `users` collection.
     * @returns Promise<Collection<Document>> Collection
     */
    _collection = async () => {
        const client = await clientPromise;
        const db = client.db("newsic");
        return db.collection("users");
    }

    /**
     * Return whether the user exists in the database or not.
     * @returns boolean
     */
    exists = async () => {
        const collection = await this._collection();
        const account = await collection.findOne({
            email: this.email
        });
        if (!account) return false;
        return true;
    }

    /**
     * Register an account for the user in the database.
     * 
     * @returns string | null User ID or null
     */
    create = async () => {
        const collection = await this._collection();
        await collection.insertOne({
            email: this.email,
            role: 'user',
            createdAt: Date.now(),
            lastLogin: 0,
        });
        const user = await collection.findOne({ email: this.email });
        if (!user) return null;
        return user._id;
    }

    /**
     * Get the role of the user.
     * @returns string
     */
    getRole = async () => {
        const collection = await this._collection();
        const account = await collection.findOne({
            email: this.email
        });
        if (!account) return null;
        return account.role;
    }

    /**
     * Update the last login timestamp of the user.
     */
    recordLoginTime = async () => {
        const collection = await this._collection();
        await collection.updateOne(
            { email: this.email },
            { $set: { lastLogin: Date.now() }}
        );
    }

    /**
     * Initiate sign-in process for the user.
     * 
     * @param token reCaptch token of the user
     */
    signin = async (token: string) => {
        await validateCaptcha(this.email, token);

        const exists = await this.exists();
        if (!exists) {
            throw Error('Authentication failed with unregistered email address.');
        }
    
        await this.token.init();
        const credentials = await this.token.create('signin', this.email, 10);
        if (!credentials) {
            throw Error('Failed to generate authentication session.');
        }
        await sendNotification({
            action: 'send_login_secret',
            session: credentials.session,
            email: this.email,
            secret: credentials.secret,
        });
    }

    /**
     * Initiate sign-up process for the user.
     * 
     * @param token reCaptch token of the user
     */
    signup = async (token: string) => {
        await validateCaptcha(this.email, token);
        const exists = await this.exists();
        if (exists) {
            return;
        }
    
        await this.token.init();
        const credentials = await this.token.create('activate', this.email, 120); // 8 hours
        if (!credentials) {
            throw new Error('Failed to generate account activation session.');
        }
        await sendNotification({
            action: 'send_account_activation',
            session: credentials.session,
            email: this.email,
            secret: credentials.secret,
            expires: 120 * 60 // Seconds
        });
    }
}

export default UserInstance;
