interface Error {
    statusCode: number;                 // HTTP response code
    errorId: string;                    // A short unique identifier for the error
    internalMessage: string;            // Error message to be processed internally
    externalMessage?: string | null;    // Error message to return to the user
}
/**
 * An error object to simplify error handling and reporting.
*/
class ServiceError implements Error {
    statusCode: number;
    errorId: string;
    internalMessage: string;
    externalMessage: string;

    /**
     * Create an error object to simplify error handling and reporting.
     * 
     * @param statusCode (number): HTTP response code
     * @param internalMessage (string): Error message to be processed internally
     * @param externalMessage (string): Error message to return to the user
    */
    constructor (statusCode: number, errorId: string, internalMessage: string, externalMessage: string) {
        this.statusCode = statusCode;
        this.errorId = errorId;
        this.internalMessage = internalMessage;
        this.externalMessage = externalMessage;
    }
}

export default ServiceError;
