import Queue from '../../lib/queue';

export const sendNotification = async (data: any) => {
    if (!process.env.QUEUE_URL || process.env.QUEUE_URL.length === 0) {
        throw Error('Fatal error: QUEUE_URL environment variable is not set.');
    }
    let queue = null;
    try {
        queue = new Queue(process.env.QUEUE_URL);
        await queue.start();
        queue.send('notification', data);
    } catch(err: any) {
        throw Error(`Failed to put request into the queue: ${err.toString()}`);
    } finally {
        if (queue) await queue.stop();
    }
}
