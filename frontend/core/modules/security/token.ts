import crypto from 'crypto';
import { get } from 'lodash';
import { Client, Entity, Schema, Repository } from 'redis-om';
import client from '../../lib/cache';

export interface TemporaryToken {
    tokenType: string;
    id: string;
    secret: string;
}

export class TemporaryToken extends Entity {}

class Token {
    client: Client | null;
    schema: Schema<TemporaryToken>;
    repository: Repository<TemporaryToken> | null;

    constructor() {
        this.client = null;
        this.repository = null;
        this.schema = new Schema(TemporaryToken, {
            tokenType: { type: 'string' },
            id: { type: 'string' },
            secret: { type: 'string' },
        });
    }

    init = async () => {
        if (this.client && this.repository) return;
        this.client = await client;
        this.repository = this.client.fetchRepository(this.schema);
        await this.repository.createIndex();
    }

    create = async (
        tokenType: string,
        email: string,
        lifeTimeInMinutes: number,
    ) => {
        if (!this.client || !this.repository) {
            throw Error('The cache repository is not initialized.');
        }
        try {
            let existingCredentials = await this.repository.search()
                .where('tokenType').is.equalTo(tokenType)
                .where('id').is.equalTo(email)
                .returnFirst();
            if (existingCredentials) {
                return {
                    tokenType: tokenType,
                    session: existingCredentials.entityId,
                    secret: existingCredentials.secret,
                }
            }
            const secret = crypto.randomBytes(64).toString('hex');
            const credentials = {
                tokenType,
                id: email,
                secret,
            };
            const res = await this.repository.createAndSave(credentials);
            const ttlInSeconds = lifeTimeInMinutes * 60; // `lifeTime` minutes
            await this.repository.expire(res.entityId, ttlInSeconds);
            return {
                tokenType,
                session: res.entityId,
                secret,
            }
        } catch (err: any) {
            console.log(`Failed to generate temporary access token: ${err.toString()}.`);
            return null;
        }
    }

    getSession = async (tokenType: string, entityId: string) => {
        if (!this.client || !this.repository) {
            throw Error('The cache repository is not initialized.');
        }
        const session = await this.repository.fetch(entityId);
        if (
            !session ||
            session.tokenType !== tokenType &&
            session.entityId !== entityId
        ) {
            return null;
        }
        return session;
    }

    isValidSecret = async (tokenType: string, entityId: string, secret: string) => {
        try {
            const session = await this.getSession(tokenType, entityId);
            const sessionSecret = get(session, ['secret'], null);
            if (sessionSecret === secret) {
                return true;
            }
            return false;
        } catch (err: any) {
            return false;
        }
    }

    getEmail = async (tokenType: string, entityId: string) => {
        try {
            const session = await this.getSession(tokenType, entityId);
            if (!session) return null;
            return session.id;
        } catch (err: any) {
            return null;
        }
    }

    clear = async (entityId: string) => {
        if (!this.client || !this.repository) {
            throw Error('The cache repository is not initialized.');
        }
        try {
            await this.repository.remove([entityId]);
        } catch (err: any) {
            console.log(`Failed to clean up authentication session: ${err.toString()}`);
            return false;
        }
    }
}

export default Token;
