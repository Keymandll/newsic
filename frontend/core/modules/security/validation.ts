import { get, isString } from 'lodash';
import { verifyCaptchaToken } from '../../../utils/captcha';
import ServiceError from '../error';

const validation = {
    email: {
        regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        errorMessage: 'Invalid email address.',
    },
};

/**
 * Check if valid is valid.
 * 
 * @param type Data type to validate (e.g. email)
 * @param value The value to validate
 * @returns boolean
 */
export const isValid = (type: string, value: string) => {
    if (value.length === 0) return false;
    const regexp = get(validation, [type, 'regexp'], null);
    if (!regexp) return false;
    return regexp.test(value);
};

/**
 * Check if email address is valid.
 * 
 * @param email Email address
 * @returns boolean
 */
export const validateEmail = (email: string) => {
    if (isString(email) && email.length > 0 && isValid('email', email)) {
        return true;
    }
    return false;
}

/**
 * Validate the reCaptcha token of the user.
 * Throws exception if the validation fails.
 * 
 * @param email Email address of the user
 * @param token reCaptcha token of the user
 */
export const validateCaptcha = async (email: string, token: string) => {
    if (!isString(token) || token.length === 0) {
        throw new ServiceError(
            400,
            'user_instance',
            `Captcha validation failed: invalid token.`,
            'Invalid request.',
        );
    }
    const result = await verifyCaptchaToken(token);
    if (!result) {
        throw new ServiceError(
            400,
            'user_instance',
            `Captcha validation failed: potential bot?`,
            'Invalid request.',
        );
    }
}
