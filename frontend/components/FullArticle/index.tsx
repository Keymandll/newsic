import dynamic from "next/dynamic";

import Link from 'next/link';
import { get } from 'lodash';
import { FC, useState } from "react";

import {
    Box,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    Typography,
    Divider,
    Tooltip,
    IconButton,
} from '@mui/material';

import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import ArticleAvatar from '../ArticleAvatar';

const Share = dynamic(() => import("../ArticleShare").then(mod => mod.default), { ssr: false });

import Article from '../../models/Article';

import { orange } from '@mui/material/colors';

const getReadTimeMinutes = (seconds: number | null) => {
    if (seconds === null) return null;
    let readTime = seconds / 60;
    if (readTime < 1) {
        return 'less than a minute';
    }
    readTime = Math.ceil(seconds / 60);
    return `${readTime} minute${readTime > 1 ? 's' : ''}`;
}

const getSavedTime = (readTime: number | null, originalReadTime: number | null) => {
    if (readTime === null || originalReadTime === null) return null;
    let saved = originalReadTime - readTime;
    return `${Math.ceil(saved / 60)} minutes saved`;
}

interface Props {
    article: Article,
}

const Article: FC<Props> = ({ article }) => {
    const [expanded, setExpanded] = useState(false);

    const publishDate = new Date(article.createdAt).toLocaleDateString("en-GB", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });

    const articleReadTime = get(article, ['statistics', 'read_time'], null);
    const sourceArticleReadTime = get(article, ['statistics', 'source_read_time'], null);

    return (
        <Card>
            <CardHeader
                avatar={
                    <ArticleAvatar article={article} toggleArticle={() => setExpanded(!expanded)} />
                }
                title={
                    <Typography
                        variant="h6"
                        onClick={() => setExpanded(!expanded)}
                        style={{ cursor: 'pointer', fontWeight: 'bold', fontSize: '1.1em' }}
                    >
                        {article.title}
                    </Typography>
                }
                subheader={publishDate}
            />
            <CardMedia
                style={{ cursor: 'pointer' }}
                component="img"
                height="194"
                image={article.image}
                alt={article.title}
                onClick={() => setExpanded(!expanded)}
            />
            <CardActions disableSpacing>
                <Tooltip title="Back to news">
                    <Link href="/">
                        <IconButton aria-label="back" style={{ color: orange[900] }}>
                        <ArrowBackIcon />
                        </IconButton>
                    </Link>
                </Tooltip>
                <Share article={article} />
                <Tooltip title="Read Original">
                    <a href={article.source} target="_blank">
                        <IconButton aria-label="source">
                            <OpenInNewIcon />
                        </IconButton>
                    </a>
                </Tooltip>
            </CardActions>
            <CardContent>
                <article>
                    <Typography paragraph>
                        {article.text}
                    </Typography>
                </article>
            </CardContent>
            <Box>
                <Divider />
                <Box p={1}>
                    <Typography style={{ fontSize: '0.8em' }}>
                        Reading time: {getReadTimeMinutes(articleReadTime)} ({getSavedTime(articleReadTime, sourceArticleReadTime)})
                    </Typography>
                </Box>
            </Box>
        </Card>
    );
}

export default Article;
