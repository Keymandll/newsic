import useSWR from 'swr';

const fetcher = (input: RequestInfo | URL, init?: RequestInit | undefined) => fetch(input, init).then((res) => res.json());

import ArticleMeta from '../ArticleMeta';

export default function LatestArticles() {
    const { data, error } = useSWR('/api/latest', fetcher);

    if (error) return <div>Failed to load</div>;
    if (!data) return <div>Loading...</div>;

    console.log(data);

    const getLatestArticles = () => {
        return data.map((item: any) => {
            return <ArticleMeta article={item} />;
        });
    }

    return (
        <div>
            {getLatestArticles()}
        </div>
    )
};
