import Footer from '../../core/components/Footer';
import FooterLeftPanel from '../FooterLeftPanel';

const NewsicFooter = () => {
    return (
        <>
            <Footer
                organizationName="Newsic"
                contentLeft={<FooterLeftPanel />}
            />
        </>
    );
}

export default NewsicFooter;
