import { useCallback, FC } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { IconButton, Tooltip } from '@mui/material';

import FilterAltIcon from '@mui/icons-material/FilterAlt';

import { setFilterEnabled } from '../../store/filterSlice';

import { orange } from '@mui/material/colors';

interface Props {}

const FilterButton: FC<Props> = () => {
    const filterEnabled: boolean = useSelector((state: any) => state.filter.filterEnabled);
    const dispatch = useDispatch();

    const enableFilter = useCallback(
        () => dispatch(setFilterEnabled(!filterEnabled)),
        [filterEnabled, dispatch]
    )

    const buttonStyle = {
        color: 'inherit',
    };
    if (filterEnabled) {
        buttonStyle.color = orange[700];
    }
    return (
        <Tooltip title="Filter">
            <IconButton
                style={buttonStyle}
                onClick={enableFilter}
            >
                <FilterAltIcon />
            </IconButton>
        </Tooltip>
    );
}

export default FilterButton;
