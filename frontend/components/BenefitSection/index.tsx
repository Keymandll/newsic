import { FC, ReactNode } from "react";

interface Props {
    icon: ReactNode,
    children?: ReactNode,
    title: string,
}

import { Box, Card, Divider, Typography } from '@mui/material';

const BenefitSection: FC<Props> = ({ icon, title, children }) => {
    return (
        <Card style={{ textAlign: 'center', minHeight: 330 }}>
            <Box p={5}>
                {icon}
                <Typography variant="h5">{title}</Typography>
                <Divider style={{ marginBottom: 25, marginTop: 20 }}/>
                <Typography>{children}</Typography>
            </Box>
        </Card>
    );
}

export default BenefitSection;
