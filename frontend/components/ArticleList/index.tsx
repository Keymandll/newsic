import { isString } from 'lodash';
import { FC } from "react";

import { Box, Typography } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2

import Article from '../Article';

interface Props {
    news: any,
    isManager: boolean,
    search?: string | undefined | null,
}

const ArticleList: FC<Props> = ({ news, isManager, search }) => {
    const message = isString(search) && search.length > 0
    ? 'We could not find anything matching the search criteria.'
    : 'There is nothing new this time.';
    if (!news || news.length === 0) {
        return (
            <Grid xs={12} md={12} lg={12} xl={12}>
                <Box
                    p={2}
                    alignContent="center"
                    alignItems="center"
                    textAlign="center"
                    style={{ minHeight: '60vh' }}
                >
                    <Typography variant="h4">Oops!</Typography>
                    <Typography style={{ marginTop: 5 }}>{message}</Typography>
                </Box>
            </Grid>
        );
    }
    return news.map((article: any, index: number) => {
        return (
            <Grid xs={12} md={6} lg={4} xl={3} key={index}>
                <Article article={article} key={index} isManager={isManager} />
            </Grid>
        );
    });
}

export default ArticleList;
