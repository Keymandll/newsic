import Link from 'next/link';

import { FC, useState } from "react";

import {
    Card,
    CardHeader,
    Typography,
} from '@mui/material';

import ArticleAvatar from '../ArticleAvatar';

import Article from '../../models/Article';

interface Props {
    article: Article,
}

const ArticleMeta: FC<Props> = ({ article }) => {
    const [expanded, setExpanded] = useState(false);

    const publishDate = new Date(article.createdAt).toLocaleDateString("en-GB", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });

    return (
        <Card>
            <CardHeader
                avatar={
                    <ArticleAvatar article={article} toggleArticle={() => setExpanded(!expanded)} />
                }
                title={
                    <Link href={`/view/${article.anchor}`}>
                        <Typography
                            variant="h6"
                            onClick={() => setExpanded(!expanded)}
                            style={{ cursor: 'pointer', fontWeight: 'bold', fontSize: '1.1em' }}
                        >
                            {article.title}
                        </Typography>
                    </Link>
                }
                subheader={publishDate}
            />
        </Card>
    );
}

export default ArticleMeta;
