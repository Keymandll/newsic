import { FC } from "react";
import { useSnackbar, VariantType } from 'notistack';

import { Tooltip, IconButton } from '@mui/material';

import ShareIcon from '@mui/icons-material/Share';

import Article from '../../models/Article';

interface Props {
    article: Article,
}

const notifyOptions = (variant: VariantType) => ({
    autoHideDuration: 1000,
    variant,
});

const ArticleShare: FC<Props> = ({ article }) => {
    const { enqueueSnackbar } = useSnackbar();
    return (
        <Tooltip title="Share">
            <IconButton aria-label="share" onClick={() => {
                navigator.clipboard.writeText(`${window.location.protocol}//${window.location.host}/view/${article.anchor}`).then(
                    () => {
                        enqueueSnackbar('Copied to clipboard.', notifyOptions('success'));
                    },
                    () => {
                        enqueueSnackbar('Failed to copy link.', notifyOptions('error'));
                    }
                );
            }}>
                <ShareIcon />
            </IconButton>
        </Tooltip>
    );
}

export default ArticleShare;
