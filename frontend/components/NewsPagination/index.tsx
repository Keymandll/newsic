import { FC } from 'react';
import { isNumber } from 'lodash';
import { useRouter } from 'next/router';

import { Box, Pagination, PaginationItem } from '@mui/material';

interface Props {
    newsCount: number,
    pagesTotal?: number,
    currentPage?: number,
}

const NewsPagination: FC<Props> = ({ newsCount, pagesTotal, currentPage }) => {
    const router = useRouter();

    const moveTo = (page: number) => {
        const query: any = { 
            ...router.query,
            page,
        };
        router.push({
            pathname: '/news',
            query,
        });
    };

    if (!isNumber(newsCount) || newsCount === 0) return null;
    if (!isNumber(pagesTotal) || pagesTotal === 0) return null;
    if (!isNumber(currentPage) || currentPage < 0) return null;
    return (
        <Box p={2} mt={2} mb={6} alignContent="center" alignItems="center" textAlign="center">
            <Pagination
                color="primary"
                size="large"
                variant="outlined"
                count={pagesTotal}
                page={currentPage}
                renderItem={(item: any) => (
                    <Box onClick={() => moveTo(item.page)}>
                        <PaginationItem {...item} />
                    </Box>
                )}
            />
        </Box>
    );
}

export default NewsPagination;
