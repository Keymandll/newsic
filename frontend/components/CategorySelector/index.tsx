import { get } from 'lodash';
import * as React from 'react';
import { FC } from "react";

import { SelectChangeEvent } from '@mui/material/Select';

import {
    OutlinedInput,
    MenuItem,
    ListItemText,
    Checkbox,
    Select,
} from '@mui/material';

import Grid from '@mui/material/Unstable_Grid2'; // Grid version 2

import SelectOption from '../../models/SelectOption';
import { Category } from '../../cache/category';

interface Props {
    value: string[],
    options: Category[],
    onChange: Function,
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const CategorySelector: FC<Props> = ({ value, options, onChange }) => {
    const [selection, setSelection] = React.useState<string[]>(value || ['all']);

    let categories: SelectOption[] = [];
    categories.push({
        value: 'all',
        text: 'Show All',
    });
    categories = categories.concat(options.map((c: Category) => {
        return {
            value: c._id,
            text: c.title,
        };
    }));

    const handleChange = (event: SelectChangeEvent<typeof selection>, child: React.ReactNode) => {
        const {
            target: { value },
        } = event;

        let values = typeof value === 'string' ? value.split(',') : value;

        if (child) {
            const lastClicked = get(child, ['props', 'value'], 'all');
            const added = !selection.includes(lastClicked);
            if (added && lastClicked === 'all') {
                setSelection(['all']);
                onChange(['all']);
                return;
            }
        }
        values = values.filter((v) => v !== 'all');
        onChange(values);
        setSelection(values);
    };

    const showSelectedItems = (selected: string[]) => {
        const selectedCategories = categories.filter((c) => selected.includes(c.value));
        return selectedCategories.map((c) => c.text).join(', ');
    };

    return (
        <Grid container spacing={2}>
            <Grid xs="auto" md="auto" lg="auto" xl="auto">
                <Select
                    sx={{ width: 300 }}
                    size="small"
                    label={null}
                    placeholder='Categories'
                    id="category-multi-checkbox"
                    multiple
                    value={selection}
                    onChange={handleChange}
                    input={<OutlinedInput />}
                    renderValue={(selected) => showSelectedItems(selected)}
                    MenuProps={MenuProps}
                >
                    {categories.map((category: SelectOption) => (
                        <MenuItem key={category.value} value={category.value}>
                            <Checkbox
                                checked={selection.indexOf(category.value) > -1}
                            />
                            <ListItemText primary={category.text} />
                        </MenuItem>
                    ))}
                </Select>
            </Grid>
        </Grid>
    );
}

export default CategorySelector;
