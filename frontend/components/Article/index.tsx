import dynamic from "next/dynamic";
import { useRouter } from 'next/router';

import Link from 'next/link';
import axios from 'axios';

import { get } from 'lodash';
import { FC, useState } from "react";

import {
    Box,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    Typography,
    Divider,
    Tooltip,
    Button,
    IconButton,
} from '@mui/material';

import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import ArticleAvatar from '../ArticleAvatar';

const Share = dynamic(() => import("../ArticleShare").then(mod => mod.default), { ssr: false });

import Article from '../../models/Article';

import { getPreviewText } from '../../utils/article';

const getReadTimeMinutes = (seconds: number | null) => {
    if (seconds === null) return null;
    let readTime = seconds / 60;
    if (readTime < 1) {
        return 'less than a minute';
    }
    readTime = Math.ceil(seconds / 60);
    return `${readTime} minute${readTime > 1 ? 's' : ''}`;
}

const getSavedTime = (readTime: number | null, originalReadTime: number | null) => {
    if (readTime === null || originalReadTime === null) return null;
    let saved = originalReadTime - readTime;
    return `${Math.ceil(saved / 60)} minutes saved`;
}

interface Props {
    article: Article,
    isManager: boolean,
}

const Article: FC<Props> = ({ article, isManager }) => {
    const router = useRouter();
    const [expanded, setExpanded] = useState(false);
    const previewText = getPreviewText(article);
    const articleText = article.text.substring(previewText.length);
    const readMore = articleText.length > previewText.length;

    const publishDate = new Date(article.createdAt).toLocaleDateString("en-GB", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });

    const articleReadTime = get(article, ['statistics', 'read_time'], null);
    const sourceArticleReadTime = get(article, ['statistics', 'source_read_time'], null);

    const showReadMoreButton = () => {
        const text = expanded ? 'Show Less' : 'Show More';
        return (
            <Button
                style={{ marginLeft: 'auto' }}
                onClick={() => setExpanded(!expanded)}
            >
                {text}
            </Button>
        );
    }

    const renderManagerActions = () => {
        if (!isManager) return null;
        return (
            <Tooltip title="Delete Article">
                <IconButton
                    onClick={async () => {
                        await axios.delete(`/api/article/${article._id}`);
                        router.reload();
                    }}
                >
                    <DeleteForeverIcon />
                </IconButton>
            </Tooltip>
        );
    }

    return (
        <Card>
            <CardHeader
                avatar={
                    <ArticleAvatar article={article} toggleArticle={() => setExpanded(!expanded)} />
                }
                title={
                    <Link href={`/view/${article.anchor}`}>
                        <Typography
                            variant="h6"
                            onClick={() => setExpanded(!expanded)}
                            style={{ cursor: 'pointer', fontWeight: 'bold', fontSize: '1.1em' }}
                        >
                            {article.title}
                        </Typography>
                    </Link>
                }
                subheader={publishDate}
            />
            <Link href={`/view/${article.anchor}`}>
                <CardMedia
                    style={{ cursor: 'pointer' }}
                    component="img"
                    height="194"
                    image={article.image}
                    alt={article.title}
                />
            </Link>
            <CardContent>
                <Box style={{ minHeight: 120 }}>
                    <Typography variant="body1" color="text.secondary">
                        {previewText} {expanded && articleText}
                    </Typography>
                </Box>
            </CardContent>
            <CardActions disableSpacing>
                <Share article={article} />
                <Tooltip title="Read Original">
                    <a href={article.source} target="_blank">
                        <IconButton aria-label="source">
                            <OpenInNewIcon />
                        </IconButton>
                    </a>
                </Tooltip>
                {renderManagerActions()}
                {readMore && showReadMoreButton()}
            </CardActions>
            <Box>
                <Divider />
                <Box p={1}>
                    <Typography style={{ fontSize: '0.8em' }}>
                        Reading time: {getReadTimeMinutes(articleReadTime)} ({getSavedTime(articleReadTime, sourceArticleReadTime)})
                    </Typography>
                </Box>
            </Box>
        </Card>
    );
}

export default Article;
