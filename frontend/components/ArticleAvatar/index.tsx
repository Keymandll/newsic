import { get } from 'lodash';
import { FC, MouseEventHandler } from "react";

import { Avatar, Tooltip } from '@mui/material';
import { orange } from '@mui/material/colors';

import Article from '../../models/Article';

const categoryColorMap = {
    politics: '#E53935',
    world: '#FFB300',
    technology: '#1E88E5',
    science: '#7CB342',
    business: '#757575',
}

const getCategoryColor = (category: string) => {
    return get(categoryColorMap, [category], orange[700]);
}

const capitalize = (text: string) => {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

interface Props {
    article: Article,
    toggleArticle: MouseEventHandler<HTMLDivElement> | undefined,
}

const ArticleAvatar: FC<Props> = ({ article, toggleArticle }) => {
    let { category } = article;
    if (!category) {
        category = '?';
    }
    return (
        <Tooltip title={capitalize(category)}>
            <Avatar
                onClick={toggleArticle}
                style={{ cursor: 'pointer' }}
                sx={{ bgcolor: getCategoryColor(category), color: '#ffffff' }}
                aria-label="article"
            >
                {category.substring(0, 1).toUpperCase()}
            </Avatar>
        </Tooltip>
    );
};

export default ArticleAvatar;
