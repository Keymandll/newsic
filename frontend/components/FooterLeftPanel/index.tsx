import { Box, Typography } from '@mui/material';

import SupportSection from '../SupportSection';

const FooterLeftPanel = () => {
    return (
        <>
            <Box>
                <img src="/logo.svg" width="18" style={{ marginRight: 5 }} />
                <Typography variant="h6" component="div" sx={{ flexGrow: 1, display: 'inline' }}>
                    ewsic: How News Meant to be Played
                </Typography>
            </Box>
            <Typography mt={1} mb={3}>Short, unopinionated and factual news powered by artificial intelligence.</Typography>
            <SupportSection />
        </>
    );
}

export default FooterLeftPanel;
