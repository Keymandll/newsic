import useSWR from 'swr';

const fetcher = (input: RequestInfo | URL, init?: RequestInit | undefined) => fetch(input, init).then((res) => res.json());

import { Box, Typography } from '@mui/material';

const getTotalReadTime = (value: number) => {
    return Math.ceil(value / 60);
}

const getTimeSaved = (value: number) => {
    return Math.ceil(value / 60);
}

const renderStatItem = (value: number, text: string) => {
    return (
        <Box>
            <Typography component="span" style={{ marginRight: 5, color: '#f57c00', fontSize: '1.2em' }}>{value}</Typography>
            <Typography component="span" style={{ fontSize: '1.2em' }}>{text}</Typography>
        </Box>
    );
};

// `value` is in seconds already
const renderStatItemTime = (value: number, text: string) => {
    let unit = 'Minute';
    if (value > 60) {
        unit = 'Hour';
        value = Math.floor(value / 60);
    }
    if (unit === 'Minutes' && value / 24 > 1) {
        unit = 'Day';
        value = Math.floor(value / 24);
    }
    if (value > 1) {
        unit += 's';
    }
    return renderStatItem(value, `${unit} ${text}`);
};

export default function Profile() {
    const { data, error } = useSWR('/api/stats', fetcher);

    if (error) return <div>Failed to load</div>;
    if (!data) return <div>Loading...</div>;

    return (
        <div>
            {renderStatItem(data.articles, 'News Articles')}
            {renderStatItemTime(getTotalReadTime(data.total_read_time), 'Total Read Time')}
            {renderStatItemTime(getTimeSaved(data.time_saved), 'Saved')}
        </div>
    )
};
