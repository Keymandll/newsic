import { useCallback } from 'react';
import axios from 'axios';
import { useSnackbar, VariantType } from 'notistack';
import { useDispatch, useSelector } from "react-redux";

import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';

import { Box, Button, TextField } from '@mui/material';

import { setVisible, setEmailAddress } from '../../store/supportSlice';

const notifyOptions = (variant: VariantType) => ({
    autoHideDuration: 3000,
    variant,
});

const SupportSection = () => {
    const { enqueueSnackbar } = useSnackbar();
    const { executeRecaptcha } = useGoogleReCaptcha();
    const visible: boolean = useSelector((state: any) => state.support.supportVisible);
    const emailAddress: string = useSelector((state: any) => state.support.emailAddress);
    const dispatch = useDispatch();

    const handleReCaptchaVerify = useCallback(
        async () => {
            if (!executeRecaptcha) {
                console.log('Execute recaptcha not yet available');
                return;
            }
            const token = await executeRecaptcha('submitSupport');

            const data = {
                email: emailAddress,
                token,
            };
            try {
                await axios.post('/api/support', data);
            } catch (err) {
                enqueueSnackbar('Something went wrong. We are looking into it.', notifyOptions('error'));
                dispatch(setVisible(true));
                return;
            }
            enqueueSnackbar('Thank you for your support!', notifyOptions('success'));
            dispatch(setEmailAddress(''));
            dispatch(setVisible(false));
        }, [emailAddress, executeRecaptcha]
    );

    const updateEmailAddress = useCallback(
        (event: any) => {
            dispatch(setEmailAddress(event.target.value));
        }, []
    );

    if (!visible) return null;
    return (
        <>
            <Box sx={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
                <TextField
                    style={{ width: '100%', maxWidth: 250, marginRight: 10 }}
                    type="email"
                    variant="outlined"
                    margin="none"
                    size="small"
                    value={emailAddress}
                    onChange={updateEmailAddress}
                    placeholder="Email address"
                />

                <Button
                    variant="contained"
                    size="medium"
                    onClick={handleReCaptchaVerify}
                >
                    Support
                </Button>
            </Box>
        </>
    );
}

export default SupportSection;
