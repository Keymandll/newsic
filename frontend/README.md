## Environment Variables

```
export REDIS_URL="redis://localhost:6379"
export DATABASE_URL="mongodb://127.0.0.1:27017/newsic"
export QUEUE_URL="amqp://127.0.0.1"
export MANAGER_EMAIL_ADDRESS="<YOUR_EMAIL_ADDRESS>"
```

## Getting Started

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
