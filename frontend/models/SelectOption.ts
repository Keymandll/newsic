interface SelectOption {
    value: string;
    text: string;
}

export default SelectOption;
