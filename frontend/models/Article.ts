export interface ArticleStatistics {
    read_time: number,
    source_read_time: number,
    time_save: number,
}

interface ArticleMetadata {
    source_host: string,
    processedAt: number,
}

export default interface Article {
    id: string,
    _id: string,
    anchor: string,
    category: string,
    title: string,
    text: string,
    tags?: string[] | undefined | null,
    image: string | undefined,
    source: string,
    statistics: ArticleStatistics | null,
    meta?: ArticleMetadata,
    createdAt: string,
}
