import { Entity, Schema } from 'redis-om';
import client from '../core/lib/cache';

export interface Category {
    _id: any;
    title: string;
    color: string;
}

export class Category extends Entity {}

const categorySchema = new Schema(Category, {
    _id: { type: 'string' },
    title: { type: 'string' },
    color: { type: 'string' },
});

export const CategoryRepository = async () => {
    const c = await client;
    const categoryRepository = c.fetchRepository(categorySchema);
    await categoryRepository.createIndex();
    return categoryRepository;
}

export const getCategories = async () => {
    const repository = await CategoryRepository();
    try {
        const cached = await repository
            .search().all();
        return cached;
    } catch (err: any) {
        return null;
    }
}
