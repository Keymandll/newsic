import { get } from 'lodash';

import type { EnvironmentConfig, SiteConfig } from './models';

const devEnv: EnvironmentConfig = {
    domain: '127.0.0.1:3000',
    logoUrl: 'http://127.0.0.1:3000/logo.svg',
    ogImageUrl: 'http://127.0.0.1:3000/ogimage.png',
}

const prodEnv: EnvironmentConfig = {
    domain: 'newsic.io',
    logoUrl: 'https://newsic.io/logo.svg',
    ogImageUrl: 'https://newsic.io/ogimage.png',
}

class Config implements SiteConfig {
    production?: EnvironmentConfig;
    development?: EnvironmentConfig;
    environment: string;

    constructor() {
        this.environment = this.getEnvironment();
        this.production = prodEnv;
        this.development = devEnv;
    }

    getEnvironment = () => {
        const env = get(process.env, ['ENVIRONMENT'], '');
        return ['production', 'development'].includes(env) ? env : 'production';
    }

    getProperty = (prop: string) => {
        return get(this, [this.environment, prop]);
    }

    getDomain = () => {
        return this.getProperty('domain');
    }

    getLogoUrl = () => {
        return this.getProperty('logoUrl');
    }

    getOgImageUrl = () => {
        return this.getProperty('ogImageUrl');
    }
}

export default new Config();
