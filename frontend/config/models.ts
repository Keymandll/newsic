export interface EnvironmentConfig {
    domain: string;
    logoUrl: string;
    ogImageUrl: string;
}

export interface SiteConfig {
    production?: EnvironmentConfig,
    development?: EnvironmentConfig,
    environment: string,
}
