import querystring from 'querystring';
import { get } from 'lodash';
import axios from 'axios';

const validation = {
    email: {
        regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        errorMessage: 'Invalid email address.',
    },
};

export const isInvalid = (type: string, value: string) => {
    if (value.length === 0) return false;
    const regexp = get(validation, [type, 'regexp'], null);
    if (!regexp) return false;
    return !regexp.test(value);
};

const verifyResponseErrorMap = {
    'missing-input-secret':     'The secret parameter is missing.',
    'invalid-input-secret':     'The secret parameter is invalid or malformed.',
    'missing-input-response':   'The response parameter is missing.',
    'invalid-input-response':   'The response parameter is invalid or malformed.',
    'bad-request':              'The request is invalid or malformed.',
    'timeout-or-duplicate':     'The response is no longer valid: either is too old or has been used previously.',
}

const checkErrors = (errorCodes: string[]) => {
    if (!Array.isArray(errorCodes) || errorCodes.length === 0) return;
    let errorsString = '';
    errorCodes.forEach((err) => {
        errorsString += `${get(verifyResponseErrorMap, err)} `;
    });
    throw Error(errorsString.trim());
}

/*
{
  "success": true|false,
  "challenge_ts": timestamp,  // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
  "hostname": string,         // the hostname of the site where the reCAPTCHA was solved
  "error-codes": [...]        // optional
}
*/
export const verifyCaptchaToken = async (response: string) => {
    try {
        const data = {
            secret: process.env['NEXT_SECRET_RECAPTCHA_SITE_KEY'] || '',
            response,
        };

        const result = await axios.post(
            'https://www.google.com/recaptcha/api/siteverify',
            querystring.stringify(data),
            {
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            }
        );
        checkErrors(result.data['error-codes']);
        return result.data.success;
    } catch(err: any) {
        console.log(`Failed to validate reCaptcha token: ${err.toString()}`);
        return false;
    }
}
