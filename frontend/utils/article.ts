import Article from '../models/Article';

export const getPreviewText = (article: Article) => {
    const text = [];
    const parts = article.text.split(".");
    for (const part of parts) {
        if (text.join(". ").length > 150) break;
        text.push(part.trim());
    }
    return `${text.join(". ")}.`;
}
